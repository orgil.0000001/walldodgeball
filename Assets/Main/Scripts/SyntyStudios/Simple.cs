﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

namespace Polygon {
    public enum SimplePack {
        SimpleAirport, SimpleApocalypse, SimpleApocalypseInteriors, SimpleBuildings, SimpleBundle, simplecar, SimpleCats, SimpleCelebrations, SimpleCitizens,
        SimpleCity, SimpleDogs, SimpleDungeons, SimpleFantasy, SimpleFantasyInteriors, SimpleFarm, SimpleFarmAnimals, SimpleForestAnimal, SimpleFX, SimpleIcons,
        SimpleInteriorsHouses, SimpleItems, SimpleMilitary, SimpleOfficeInteriors, SimplePeople, SimplePeople2, SimplePeople3, SimplePort, SimpleProps, SimpleRacer,
        SimpleRoadwork, SimpleShopInteriors, SimpleSky, SimpleSpace, SimpleSpaceCharacters, SimpleSpaceInteriors, SimpleTemple, SimpleTown, SimpleTownLite,
        SimpleTrains, Simple__Vehicle__Pack, SimpleWorld_VolumeTwo, SimpleWorldVol1, SimpleZombies,
    }
    public class Simple {
    }
}