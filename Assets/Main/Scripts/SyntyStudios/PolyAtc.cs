﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

namespace Polygon {
    public class PolyAtcData {
        public PolyBodyPart part;
        public Tf tf = new Tf();
        public bool isCrt = true;
        public PolyAtcData show => Show();
        public PolyAtcData C => new PolyAtcData(this);
        public PolyAtcData(PolyBodyPart part) { this.part = part; }
        public PolyAtcData(PolyAtcData data) {
            part = data.part;
            tf = data.tf.C;
            isCrt = data.isCrt;
        }
        PolyAtcData Show() { this.isCrt = false; return this; }
        public PolyAtcData P(float x, float y, float z) { tf.p = V3.V(x, y, z); return this; }
        public PolyAtcData Pxy(float x, float y) { tf.p = tf.p.Xy(x, y); return this; }
        public PolyAtcData Pxz(float x, float z) { tf.p = tf.p.Xz(x, z); return this; }
        public PolyAtcData Pyz(float y, float z) { tf.p = tf.p.Yz(y, z); return this; }
        public PolyAtcData Px(float x) { tf.p = tf.p.X(x); return this; }
        public PolyAtcData Py(float y) { tf.p = tf.p.Y(y); return this; }
        public PolyAtcData Pz(float z) { tf.p = tf.p.Z(z); return this; }
        public PolyAtcData R(float x, float y, float z) { tf.r = V3.V(x, y, z); return this; }
        public PolyAtcData Rxy(float x, float y) { tf.r = tf.r.Xy(x, y); return this; }
        public PolyAtcData Rxz(float x, float z) { tf.r = tf.r.Xz(x, z); return this; }
        public PolyAtcData Ryz(float y, float z) { tf.r = tf.r.Yz(y, z); return this; }
        public PolyAtcData Rx(float x) { tf.r = tf.r.X(x); return this; }
        public PolyAtcData Ry(float y) { tf.r = tf.r.Y(y); return this; }
        public PolyAtcData Rz(float z) { tf.r = tf.r.Z(z); return this; }
        public PolyAtcData S(float x, float y, float z) { tf.s = V3.V(x, y, z); return this; }
        public PolyAtcData S(float v) { tf.s = V3.V(v); return this; }
    }
    public enum PolyAtc {
        // PolygonAdventure
        PolygonApocalypse_SM_Chr_Attach_Armour_Knee_Metal_L_01, PolygonApocalypse_SM_Chr_Attach_Armour_Knee_Metal_R_01, PolygonApocalypse_SM_Chr_Attach_Armour_Knee_Sports_L_01, PolygonApocalypse_SM_Chr_Attach_Armour_Knee_Sports_R_01, PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Metal_L_01, PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Metal_L_02, PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Metal_R_01, PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Metal_R_02, PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Soft_L_01, PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Soft_R_01, PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Sports_L_01, PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Sports_L_02, PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Sports_R_01, PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Sports_R_02, PolygonApocalypse_SM_Chr_Attach_Armour_Thigh_Metal_L_01, PolygonApocalypse_SM_Chr_Attach_Armour_Thigh_Metal_R_01, PolygonApocalypse_SM_Chr_Attach_Armour_Wrist_Metal_L_01, PolygonApocalypse_SM_Chr_Attach_Armour_Wrist_Metal_R_01, PolygonApocalypse_SM_Chr_Attach_Backpack_01, PolygonApocalypse_SM_Chr_Attach_Backpack_02, PolygonApocalypse_SM_Chr_Attach_Backpack_Large_01, PolygonApocalypse_SM_Chr_Attach_Backpack_Large_Extras_01, PolygonApocalypse_SM_Chr_Attach_Backpack_Small_01, PolygonApocalypse_SM_Chr_Attach_Bedroll_01, PolygonApocalypse_SM_Chr_Attach_Biker_Male_Beard_01, PolygonApocalypse_SM_Chr_Attach_Biker_Male_Hair_01, PolygonApocalypse_SM_Chr_Attach_Bullets_01, PolygonApocalypse_SM_Chr_Attach_Business_Male_Hair_01, PolygonApocalypse_SM_Chr_Attach_Canteen_01, PolygonApocalypse_SM_Chr_Attach_Cool_Female_Hair_01, PolygonApocalypse_SM_Chr_Attach_Cool_Male_Hair_01, PolygonApocalypse_SM_Chr_Attach_Criminal_Male_Beard_01, PolygonApocalypse_SM_Chr_Attach_Criminal_Male_Hair_01, PolygonApocalypse_SM_Chr_Attach_Emo_Female_Hair_01, PolygonApocalypse_SM_Chr_Attach_Emo_Female_Spikes_L_01, PolygonApocalypse_SM_Chr_Attach_Emo_Female_Spikes_R_01, PolygonApocalypse_SM_Chr_Attach_FootballHelmet_01, PolygonApocalypse_SM_Chr_Attach_GasMask_01, PolygonApocalypse_SM_Chr_Attach_Hazmat_Male_Glass_01, PolygonApocalypse_SM_Chr_Attach_Homeless_Male_Beard_01, PolygonApocalypse_SM_Chr_Attach_Hunter_Male_Beard_01, PolygonApocalypse_SM_Chr_Attach_Hunter_Male_Hat_01, PolygonApocalypse_SM_Chr_Attach_Hunter_Male_Hat_02, PolygonApocalypse_SM_Chr_Attach_Islander_Male_Hair_01, PolygonApocalypse_SM_Chr_Attach_Knife_01, PolygonApocalypse_SM_Chr_Attach_Mags_01, PolygonApocalypse_SM_Chr_Attach_Mags_02, PolygonApocalypse_SM_Chr_Attach_Mags_03, PolygonApocalypse_SM_Chr_Attach_Mask_01, PolygonApocalypse_SM_Chr_Attach_Mask_Hockey_01, PolygonApocalypse_SM_Chr_Attach_Mechanic_Female_Hair_01, PolygonApocalypse_SM_Chr_Attach_Nerd_Female_Glasses_01, PolygonApocalypse_SM_Chr_Attach_Nerd_Female_Hair_01, PolygonApocalypse_SM_Chr_Attach_Nun_Female_HipPouch_01, PolygonApocalypse_SM_Chr_Attach_Patient_Female_Hair_01, PolygonApocalypse_SM_Chr_Attach_Pouch_01, PolygonApocalypse_SM_Chr_Attach_Pouch_02, PolygonApocalypse_SM_Chr_Attach_Pouch_03, PolygonApocalypse_SM_Chr_Attach_Press_Male_Beard_01, PolygonApocalypse_SM_Chr_Attach_Press_Male_Eyepatch_01, PolygonApocalypse_SM_Chr_Attach_Press_Male_Hair_01, PolygonApocalypse_SM_Chr_Attach_Punk_Female_Hair_01, PolygonApocalypse_SM_Chr_Attach_RiotCop_Male_Beard_01, PolygonApocalypse_SM_Chr_Attach_RiotCop_Male_Hair_01, PolygonApocalypse_SM_Chr_Attach_RiotCop_Male_Helmet_01, PolygonApocalypse_SM_Chr_Attach_RiotCop_Male_Radio_01, PolygonApocalypse_SM_Chr_Attach_Scout_Female_Hat_01, PolygonApocalypse_SM_Chr_Attach_Scout_Female_Radio_01, PolygonApocalypse_SM_Chr_Attach_Sheriff_Male_Hair_01, PolygonApocalypse_SM_Chr_Attach_Sheriff_Male_Hat_01, PolygonApocalypse_SM_Chr_Attach_Soldier_Female_Hair_01, PolygonApocalypse_SM_Chr_Attach_Soldier_Male_Glass_01, PolygonApocalypse_SM_Chr_Attach_Soldier_Male_Helmet_01, PolygonApocalypse_SM_Chr_Attach_SupplyBag_01, PolygonApocalypse_SM_Chr_Attach_Teen_Female_Glasses_01, PolygonApocalypse_SM_Chr_Attach_Teen_Female_Hair_01, PolygonApocalypse_SM_Chr_Attach_Teen_Male_Hat_01, PolygonApocalypse_SM_Chr_Attach_Waitress_Female_Glasses_01, PolygonApocalypse_SM_Chr_Attach_Waitress_Female_Hair_01, PolygonApocalypse_SM_Chr_Attach_Wanderer_Male_Beard_01, PolygonApocalypse_SM_Chr_Attach_Wanderer_Male_Hair_01, PolygonApocalypse_SM_Chr_Attach_Zombie_Female_Hair_01, PolygonApocalypse_SM_Chr_Attach_Zombie_Female_Hair_02, PolygonApocalypse_SM_Chr_Attach_Zombie_Male_Beard_01, PolygonApocalypse_SM_Chr_Attach_Zombie_Male_Hair_01, PolygonApocalypse_SM_Chr_Attach_Zombie_Male_Hair_02,
        PolygonBattleRoyale_SM_Char_Attach_Female_Armor_01, PolygonBattleRoyale_SM_Char_Attach_Female_Armor_02, PolygonBattleRoyale_SM_Char_Attach_Female_Armor_03, PolygonBattleRoyale_SM_Char_Attach_Male_Armor_01, PolygonBattleRoyale_SM_Char_Attach_Male_Armor_02, PolygonBattleRoyale_SM_Char_Attach_Male_Armor_03, PolygonBattleRoyale_SM_Char_Attach_Bag_Lvl1, PolygonBattleRoyale_SM_Char_Attach_Bag_Lvl2, PolygonBattleRoyale_SM_Char_Attach_Bag_Lvl3, PolygonBattleRoyale_SM_Char_Attach_Helmet_01, PolygonBattleRoyale_SM_Char_Attach_Helmet_02, PolygonBattleRoyale_SM_Char_Attach_Helmet_03, PolygonBattleRoyale_SM_Chr_Attach_Ammo_01, PolygonBattleRoyale_SM_Chr_Attach_Beard_02, PolygonBattleRoyale_SM_Chr_Attach_Beard_03, PolygonBattleRoyale_SM_Chr_Attach_Beard_04, PolygonBattleRoyale_SM_Chr_Attach_Beard_05, PolygonBattleRoyale_SM_Chr_Attach_Beard_06, PolygonBattleRoyale_SM_Chr_Attach_Beard_07, PolygonBattleRoyale_SM_Chr_Attach_Bedroll_01, PolygonBattleRoyale_SM_Chr_Attach_Earmuffs_01, PolygonBattleRoyale_SM_Chr_Attach_Eyepatch_01, PolygonBattleRoyale_SM_Chr_Attach_Facemask_01, PolygonBattleRoyale_SM_Chr_Attach_Facemask_02, PolygonBattleRoyale_SM_Chr_Attach_Facemask_03, PolygonBattleRoyale_SM_Chr_Attach_Female_01, PolygonBattleRoyale_SM_Chr_Attach_Female_02, PolygonBattleRoyale_SM_Chr_Attach_Female_Default_Hair_01, PolygonBattleRoyale_SM_Chr_Attach_Female_Hair_01, PolygonBattleRoyale_SM_Chr_Attach_Female_Hair_Pigtails_01, PolygonBattleRoyale_SM_Chr_Attach_GasMask_01, PolygonBattleRoyale_SM_Chr_Attach_Glasses_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Bandana_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Beanie_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Beer_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Bunny_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Captian_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Cap_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Cowboy_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Crown_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Cupcake_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Elf_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Fez_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_FlatTop_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Football_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_IceCream_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Irish_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Knight_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Mexican_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Mil_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Party_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Pot_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Racoon_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Roadcone_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Roman_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Samurai_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Shark_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Squid_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Tacticle_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Turban_01, PolygonBattleRoyale_SM_Chr_Attach_Hat_Unicorn_01, PolygonBattleRoyale_SM_Chr_Attach_Male_Default_Hair_01, PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_01, PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_02, PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_03, PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_04, PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_05, PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_06, PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_07, PolygonBattleRoyale_SM_Chr_Attach_Pouch_01, PolygonBattleRoyale_SM_Chr_Attach_Pouch_02, PolygonBattleRoyale_SM_Chr_Attach_Pouch_03, PolygonBattleRoyale_SM_Chr_Attach_Pouch_04, PolygonBattleRoyale_SM_Chr_Attach_Pouch_05, PolygonBattleRoyale_SM_Chr_Attach_Pouch_06, PolygonBattleRoyale_SM_Chr_Attach_Pouch_07, PolygonBattleRoyale_SM_Chr_Attach_Pouch_08, PolygonBattleRoyale_SM_Chr_Attach_Scarf_01, PolygonBattleRoyale_SM_Chr_Attach_Scarf_02, PolygonBattleRoyale_SM_Chr_Patch_Flag_01, PolygonBattleRoyale_SM_Chr_Patch_Leaf_01, PolygonBattleRoyale_SM_Chr_Patch_Medic_01, PolygonBattleRoyale_SM_Chr_Patch_Medic_02, PolygonBattleRoyale_SM_Chr_Patch_Rank_01, PolygonBattleRoyale_SM_Chr_Patch_Rank_02, PolygonBattleRoyale_SM_Chr_Patch_Rank_03, PolygonBattleRoyale_SM_Chr_Patch_Star_01, PolygonBattleRoyale_SM_Chr_Patch_Star_02,
        PolygonBossZombies_SM_Prop_Cigarette_01, PolygonBossZombies_SM_Prop_HeartGlasses_01, PolygonBossZombies_SM_Prop_Ribbon_01, PolygonBossZombies_SM_Chr_Attach_Blobber_Hair_01, PolygonBossZombies_SM_Chr_Attach_Blobber_Hair_02, PolygonBossZombies_SM_Chr_Attach_Blobber_Tiara_01, PolygonBossZombies_SM_Chr_Attach_Blobber_Tiara_02, PolygonBossZombies_SM_Chr_Attach_Brute_Bandana_01, PolygonBossZombies_SM_Chr_Attach_Brute_Extra_01, PolygonBossZombies_SM_Chr_Attach_Brute_Hair_01, PolygonBossZombies_SM_Chr_Attach_Brute_Hair_02, PolygonBossZombies_SM_Chr_Attach_Slobber_FacialHair_01, PolygonBossZombies_SM_Chr_Attach_Slobber_Hair_01, PolygonBossZombies_SM_Chr_Attach_Wretch_Hair_01, PolygonBossZombies_SM_Chr_Attach_Wretch_Hair_02,
        // PolygonCity
        // POLYGONCityCharacters
        PolygonConstruction_SM_Chr_Attach_Beard_01, PolygonConstruction_SM_Chr_Attach_Beard_02, PolygonConstruction_SM_Chr_Attach_Beard_03, PolygonConstruction_SM_Chr_Attach_Beard_Grey_01, PolygonConstruction_SM_Chr_Attach_Beard_Grey_02, PolygonConstruction_SM_Chr_Attach_BeltLoop_01, PolygonConstruction_SM_Chr_Attach_BeltLoop_Chisel_01, PolygonConstruction_SM_Chr_Attach_BeltLoop_Hammer_01, PolygonConstruction_SM_Chr_Attach_BeltLoop_Screwdriver_01, PolygonConstruction_SM_Chr_Attach_Cigarette_01, PolygonConstruction_SM_Chr_Attach_Cigarette_02, PolygonConstruction_SM_Chr_Attach_Earmuffs_01, PolygonConstruction_SM_Chr_Attach_FaceShield_Mesh_01, PolygonConstruction_SM_Chr_Attach_FaceShield_Plastic_01, PolygonConstruction_SM_Chr_Attach_FilterMask_01, PolygonConstruction_SM_Chr_Attach_Goatee_01, PolygonConstruction_SM_Chr_Attach_Goggles_01, PolygonConstruction_SM_Chr_Attach_Goggles_OnHead_01, PolygonConstruction_SM_Chr_Attach_Hat_01, PolygonConstruction_SM_Chr_Attach_Helmet_01, PolygonConstruction_SM_Chr_Attach_Helmet_02, PolygonConstruction_SM_Chr_Attach_Helmet_Backwards_01, PolygonConstruction_SM_Chr_Attach_Helmet_Earmuffs_01, PolygonConstruction_SM_Chr_Attach_Helmet_Earmuffs_02, PolygonConstruction_SM_Chr_Attach_Helmet_Goggles_01, PolygonConstruction_SM_Chr_Attach_Helmet_Hat_01, PolygonConstruction_SM_Chr_Attach_ID_01, PolygonConstruction_SM_Chr_Attach_Mustache_01, PolygonConstruction_SM_Chr_Attach_Pouch_01, PolygonConstruction_SM_Chr_Attach_Pouch_02, PolygonConstruction_SM_Chr_Attach_SafteyGlasses_01, PolygonConstruction_SM_Chr_Attach_SunGlasses_01, PolygonConstruction_SM_Chr_Attach_WelderMask_01, PolygonConstruction_SM_Chr_Inspector_Female_Glasses_01, PolygonConstruction_SM_Chr_Inspector_Male_Glasses_01,
        // PolygonDungeon
        // PolygonExplorers
        PolygonExplorers_SM_Chr_Attach_Backpack_01, PolygonExplorers_SM_Chr_Attach_Beard_01, PolygonExplorers_SM_Chr_Attach_Hair_Female_01, PolygonExplorers_SM_Chr_Attach_Hair_Female_Hat_01, PolygonExplorers_SM_Chr_Attach_Hair_Male_01, PolygonExplorers_SM_Chr_Attach_Hair_Male_Hat_01, PolygonExplorers_SM_Chr_Attach_Hat_Female_01, PolygonExplorers_SM_Chr_Attach_Hat_Hunter_01, PolygonExplorers_SM_Chr_Attach_Hat_Male_01, PolygonExplorers_SM_Chr_Attach_Pouch_01, PolygonExplorers_SM_Chr_Attach_Rope_01, PolygonExplorers_SM_Chr_Attach_Whip_01,
        // PolygonFantasyCharacters
        // PolygonFantasyHeroCharacters
        PolygonFantasyKingdom_SM_Chr_Attach_Blacksmith_Female_Goggles_02, PolygonFantasyKingdom_SM_Chr_Attach_Blacksmith_Male_Goggles_01, PolygonFantasyKingdom_SM_Chr_Attach_Blacksmith_Male_Goggles_02, PolygonFantasyKingdom_SM_Chr_Attach_King_Cape_01, PolygonFantasyKingdom_SM_Chr_Attach_King_Crown_01, PolygonFantasyKingdom_SM_Chr_Attach_Mage_Cape_01, PolygonFantasyKingdom_SM_Chr_Attach_Priest_Hat_01, PolygonFantasyKingdom_SM_Chr_Attach_Princess_Tiara_01, PolygonFantasyKingdom_SM_Chr_Attach_Queen_Crown_01, PolygonFantasyKingdom_SM_Chr_Attach_Soldier_01,
        // PolygonFantasyRivals
        PolygonFarm_SM_Chr_Attach_Buckethat_01___lp_1_rp_, PolygonFarm_SM_Chr_Attach_Cap_01, PolygonFarm_SM_Chr_Attach_Cap_02, PolygonFarm_SM_Chr_Attach_CowboyHat_01, PolygonFarm_SM_Chr_Attach_CowboyHat_02, PolygonFarm_SM_Chr_Attach_Glasses_01, PolygonFarm_SM_Chr_Attach_ScarecrowHat_01, PolygonFarm_SM_Chr_Attach_Wheat_01,
        // PolygonGangWarfare
        PolygonHeist_SM_Item_Hair_Eyebrow_Man_01, PolygonHeist_SM_Item_Hair_Eyebrow_Woman_01, PolygonHeist_SM_Item_Hair_Man_01, PolygonHeist_SM_Item_Hair_Woman_01, PolygonHeist_SM_Item_Mask_Alien_01, PolygonHeist_SM_Item_Mask_Balaclava_01, PolygonHeist_SM_Item_Mask_Beanie_01, PolygonHeist_SM_Item_Mask_Chicken_01, PolygonHeist_SM_Item_Mask_Clown_01, PolygonHeist_SM_Item_Mask_Construction_01, PolygonHeist_SM_Item_Mask_Fox_01, PolygonHeist_SM_Item_Mask_Gas_01, PolygonHeist_SM_Item_Mask_Glasses_01, PolygonHeist_SM_Item_Mask_Hat_01, PolygonHeist_SM_Item_Mask_Hockey_01, PolygonHeist_SM_Item_Mask_Horse_01, PolygonHeist_SM_Item_Mask_Luchador_01, PolygonHeist_SM_Item_Mask_Panda_01, PolygonHeist_SM_Item_Mask_PaperBag_01, PolygonHeist_SM_Item_Mask_Simple_Man_01, PolygonHeist_SM_Item_Mask_Simple_Robber_01, PolygonHeist_SM_Item_Mask_Simple_Woman_01, PolygonHeist_SM_Item_Mask_Tiger_01, PolygonHeist_SM_Item_Mask_Trump_01, PolygonHeist_SM_Item_Mask_WeldersMask_01,
        // PolygonKnights
        PolygonMilitary_SM_Chr_Attach_Nameplate_01, PolygonMilitary_SM_Chr_Attach_Nameplate_02, PolygonMilitary_SM_Chr_Attach_Nameplate_03, PolygonMilitary_SM_Chr_Attach_Nameplate_04, PolygonMilitary_SM_Chr_Attach_Nameplate_05, PolygonMilitary_SM_Chr_Attach_Nameplate_06, PolygonMilitary_SM_Chr_Attach_Nameplate_07, PolygonMilitary_SM_Chr_Attach_Nameplate_08, PolygonMilitary_SM_Chr_Attach_Nameplate_09, PolygonMilitary_SM_Chr_Attach_Nameplate_10, PolygonMilitary_SM_Chr_Attach_Patch_Flag_01, PolygonMilitary_SM_Chr_Attach_Patch_Flag_02, PolygonMilitary_SM_Chr_Attach_Patch_Flag_03, PolygonMilitary_SM_Chr_Attach_Patch_Flag_04, PolygonMilitary_SM_Chr_Attach_Patch_Flag_05, PolygonMilitary_SM_Chr_Attach_Patch_Flag_06, PolygonMilitary_SM_Chr_Attach_Patch_Flag_07, PolygonMilitary_SM_Chr_Attach_Patch_Flag_08, PolygonMilitary_SM_Chr_Attach_Patch_Flag_09, PolygonMilitary_SM_Chr_Attach_Patch_Flag_10, PolygonMilitary_SM_Chr_Attach_Patch_Flag_11, PolygonMilitary_SM_Chr_Attach_Patch_Flag_12, PolygonMilitary_SM_Chr_Attach_Patch_Rank_01, PolygonMilitary_SM_Chr_Attach_Patch_Rank_02, PolygonMilitary_SM_Chr_Attach_Patch_Rank_03, PolygonMilitary_SM_Chr_Attach_Patch_Rank_04, PolygonMilitary_SM_Chr_Attach_Patch_Rank_05, PolygonMilitary_SM_Chr_Attach_Patch_Rank_06, PolygonMilitary_SM_Chr_Attach_Patch_Rank_07, PolygonMilitary_SM_Chr_Attach_Patch_Rank_08, PolygonMilitary_SM_Chr_Attach_Patch_Rank_09, PolygonMilitary_SM_Chr_Attach_Patch_Squad_01, PolygonMilitary_SM_Chr_Attach_Patch_Squad_02, PolygonMilitary_SM_Chr_Attach_Patch_Squad_03, PolygonMilitary_SM_Chr_Attach_Patch_Squad_04, PolygonMilitary_SM_Chr_Attach_Patch_Squad_05, PolygonMilitary_SM_Chr_Attach_Patch_Squad_06, PolygonMilitary_SM_Chr_Attach_Patch_Squad_07, PolygonMilitary_SM_Chr_Attach_Patch_Squad_08, PolygonMilitary_SM_Chr_Attach_Patch_Squad_09, PolygonMilitary_SM_Char_Attach_Beret_01, PolygonMilitary_SM_Char_Attach_Pilot_Helmet_01, PolygonMilitary_SM_Chr_Attach_Backpack_01, PolygonMilitary_SM_Chr_Attach_Backpack_02, PolygonMilitary_SM_Chr_Attach_Battery_01, PolygonMilitary_SM_Chr_Attach_Beanie_01, PolygonMilitary_SM_Chr_Attach_Beard_01, PolygonMilitary_SM_Chr_Attach_Beard_02, PolygonMilitary_SM_Chr_Attach_Beard_03, PolygonMilitary_SM_Chr_Attach_Beard_04, PolygonMilitary_SM_Chr_Attach_Beard_05, PolygonMilitary_SM_Chr_Attach_Beard_06, PolygonMilitary_SM_Chr_Attach_Beard_07, PolygonMilitary_SM_Chr_Attach_Beard_08, PolygonMilitary_SM_Chr_Attach_Beard_09, PolygonMilitary_SM_Chr_Attach_Beard_10, PolygonMilitary_SM_Chr_Attach_Beard_11, PolygonMilitary_SM_Chr_Attach_Beard_12, PolygonMilitary_SM_Chr_Attach_Bombsuit_Helmet_01, PolygonMilitary_SM_Chr_Attach_Bombsuit_Wrist_01, PolygonMilitary_SM_Chr_Attach_Bomb_C4_01, PolygonMilitary_SM_Chr_Attach_Bomb_C4_02, PolygonMilitary_SM_Chr_Attach_Bomb_Dynamite_01, PolygonMilitary_SM_Chr_Attach_Bomb_Dynamite_02, PolygonMilitary_SM_Chr_Attach_Bomb_Kit_01, PolygonMilitary_SM_Chr_Attach_Book_Pouch_01, PolygonMilitary_SM_Chr_Attach_Book_Pouch_02, PolygonMilitary_SM_Chr_Attach_Chin_Strap_01_Female, PolygonMilitary_SM_Chr_Attach_Chin_Strap_01_Male, PolygonMilitary_SM_Chr_Attach_Chin_Strap_02_Female, PolygonMilitary_SM_Chr_Attach_Chin_Strap_02_Male, PolygonMilitary_SM_Chr_Attach_CowboyHat_01, PolygonMilitary_SM_Chr_Attach_Earmuffs_01, PolygonMilitary_SM_Chr_Attach_Eyepatch_01, PolygonMilitary_SM_Chr_Attach_Gas_Mask_01, PolygonMilitary_SM_Chr_Attach_Gas_Mask_01_Strap, PolygonMilitary_SM_Chr_Attach_Glasses_01, PolygonMilitary_SM_Chr_Attach_Glasses_02, PolygonMilitary_SM_Chr_Attach_Glasses_03, PolygonMilitary_SM_Chr_Attach_Glowstick_01, PolygonMilitary_SM_Chr_Attach_Goggles_01, PolygonMilitary_SM_Chr_Attach_Goggles_Strap_01, PolygonMilitary_SM_Chr_Attach_Grenade_01, PolygonMilitary_SM_Chr_Attach_Grenade_Flash_01, PolygonMilitary_SM_Chr_Attach_Grenade_Smoke_01, PolygonMilitary_SM_Chr_Attach_Hair_Female_01, PolygonMilitary_SM_Chr_Attach_Hair_Female_02, PolygonMilitary_SM_Chr_Attach_Hair_Female_03, PolygonMilitary_SM_Chr_Attach_Hair_Female_04, PolygonMilitary_SM_Chr_Attach_Hair_Female_05, PolygonMilitary_SM_Chr_Attach_Hair_Male_01, PolygonMilitary_SM_Chr_Attach_Hair_Male_02, PolygonMilitary_SM_Chr_Attach_Hair_Male_03, PolygonMilitary_SM_Chr_Attach_Hair_Male_04, PolygonMilitary_SM_Chr_Attach_Hair_Male_05, PolygonMilitary_SM_Chr_Attach_Hair_Male_06, PolygonMilitary_SM_Chr_Attach_Hair_Male_07, PolygonMilitary_SM_Chr_Attach_Hair_Male_08, PolygonMilitary_SM_Chr_Attach_Hair_Male_09, PolygonMilitary_SM_Chr_Attach_Hair_Male_10, PolygonMilitary_SM_Chr_Attach_Hair_Male_11, PolygonMilitary_SM_Chr_Attach_Hat_01, PolygonMilitary_SM_Chr_Attach_Hat_02, PolygonMilitary_SM_Chr_Attach_Hat_03, PolygonMilitary_SM_Chr_Attach_Hat_04, PolygonMilitary_SM_Chr_Attach_Hat_05, PolygonMilitary_SM_Chr_Attach_Hat_06, PolygonMilitary_SM_Chr_Attach_Hat_07, PolygonMilitary_SM_Chr_Attach_Hat_Cap_01, PolygonMilitary_SM_Chr_Attach_Hat_Cap_02, PolygonMilitary_SM_Chr_Attach_Headset_01, PolygonMilitary_SM_Chr_Attach_Helmet_01, PolygonMilitary_SM_Chr_Attach_Helmet_01_Goggles_01, PolygonMilitary_SM_Chr_Attach_Helmet_01_Goggles_02, PolygonMilitary_SM_Chr_Attach_Helmet_01_Goggles_03, PolygonMilitary_SM_Chr_Attach_Helmet_02, PolygonMilitary_SM_Chr_Attach_Helmet_03, PolygonMilitary_SM_Chr_Attach_Helmet_04, PolygonMilitary_SM_Chr_Attach_Helmet_05, PolygonMilitary_SM_Chr_Attach_Helmet_06, PolygonMilitary_SM_Chr_Attach_Helmet_07, PolygonMilitary_SM_Chr_Attach_Helmet_08, PolygonMilitary_SM_Chr_Attach_Helmet_09, PolygonMilitary_SM_Chr_Attach_Helmet_10, PolygonMilitary_SM_Chr_Attach_Helmet_11_Female, PolygonMilitary_SM_Chr_Attach_Helmet_11_Male, PolygonMilitary_SM_Chr_Attach_Holster_01, PolygonMilitary_SM_Chr_Attach_Holster_Pistol_01, PolygonMilitary_SM_Chr_Attach_Holster_Pistol_02, PolygonMilitary_SM_Chr_Attach_Holster_Pistol_03, PolygonMilitary_SM_Chr_Attach_Knife_Sheath_01, PolygonMilitary_SM_Chr_Attach_Mustache_01, PolygonMilitary_SM_Chr_Attach_NVG_01, PolygonMilitary_SM_Chr_Attach_NVG_02, PolygonMilitary_SM_Chr_Attach_NVG_03, PolygonMilitary_SM_Chr_Attach_NVG_Mount_01, PolygonMilitary_SM_Chr_Attach_Padding_01, PolygonMilitary_SM_Chr_Attach_Pouch_01, PolygonMilitary_SM_Chr_Attach_Pouch_02, PolygonMilitary_SM_Chr_Attach_Pouch_03, PolygonMilitary_SM_Chr_Attach_Pouch_04, PolygonMilitary_SM_Chr_Attach_Pouch_05, PolygonMilitary_SM_Chr_Attach_Pouch_06, PolygonMilitary_SM_Chr_Attach_Pouch_07, PolygonMilitary_SM_Chr_Attach_Pouch_08, PolygonMilitary_SM_Chr_Attach_Pouch_09, PolygonMilitary_SM_Chr_Attach_Pouch_Grenade_01, PolygonMilitary_SM_Chr_Attach_Pouch_Large_01, PolygonMilitary_SM_Chr_Attach_Pouch_Mag_01, PolygonMilitary_SM_Chr_Attach_Pouch_Mag_02, PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Double_01, PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Double_02, PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Pistol_01, PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Single_01, PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Single_02, PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Single_Handle_01, PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Single_Handle_02, PolygonMilitary_SM_Chr_Attach_Pouch_WalkieTalkie_01, PolygonMilitary_SM_Chr_Attach_Pouch_WalkieTalkie_Empty_01, PolygonMilitary_SM_Chr_Attach_Radio_01, PolygonMilitary_SM_Chr_Attach_ShellStrip_Rifle_01, PolygonMilitary_SM_Chr_Attach_ShellStrip_Shotgun_01, PolygonMilitary_SM_Chr_Attach_SunGlasses_01_Female, PolygonMilitary_SM_Chr_Attach_SunGlasses_01_Male, PolygonMilitary_SM_Chr_Attach_Tool_01, PolygonMilitary_SM_Chr_Attach_Tool_02, PolygonMilitary_SM_Chr_Attach_Torch_01, PolygonMilitary_SM_Chr_Attach_Turban_01, PolygonMilitary_SM_Chr_Attach_Turban_02,
        PolygonOffice_SM_Chr_Attachment_Antlers_01, PolygonOffice_SM_Chr_Attachment_Headset_01, PolygonOffice_SM_Chr_Attachment_Headset_02, PolygonOffice_SM_Chr_Attachment_Identity_01, PolygonOffice_SM_Chr_Attachment_Nametag_01, PolygonOffice_SM_Chr_Attachment_Pens_01,
        // PolygonPirates
        // PolygonPrototype
        // PolygonSamurai
        // PolygonSciFiCity
        PolygonSciFiSpace_SM_Chr_Attach_SpaceHelmet_01, PolygonSciFiSpace_SM_Chr_Attach_Crew_Female_Hair_01, PolygonSciFiSpace_SM_Chr_Attach_Crew_Male_Hair_01, PolygonSciFiSpace_SM_Chr_Attach_EVA_Cover_01, PolygonSciFiSpace_SM_Chr_Attach_EVA_Cover_Clear_01, PolygonSciFiSpace_SM_Chr_Attach_Hat_Captain_01, PolygonSciFiSpace_SM_Chr_Attach_Hat_Crew_01, PolygonSciFiSpace_SM_Chr_Attach_Hunter_Female_Hair_01, PolygonSciFiSpace_SM_Chr_Attach_Junker_Hair_Female_01, PolygonSciFiSpace_SM_Chr_Attach_Junker_Headset_01, PolygonSciFiSpace_SM_Chr_Attach_Junker_Helmet_01, PolygonSciFiSpace_SM_Chr_Attach_Junker_Helmet_Glass_01, PolygonSciFiSpace_SM_Chr_Attach_Male_Hair_01, PolygonSciFiSpace_SM_Chr_Attach_Male_Hair_02, PolygonSciFiSpace_SM_Chr_Attach_Male_Hair_03, PolygonSciFiSpace_SM_Chr_Attach_Male_Hair_05, PolygonSciFiSpace_SM_Chr_Attach_Male_Hair_06, PolygonSciFiSpace_SM_Chr_Attach_Male_Hair_07, PolygonSciFiSpace_SM_Chr_Attach_Mask_Medic_01,
        PolygonSnow_SM_Attachment_Balaclava_01, PolygonSnow_SM_Attachment_Beanie_01, PolygonSnow_SM_Attachment_Beanie_02, PolygonSnow_SM_Attachment_Beanie_03, PolygonSnow_SM_Attachment_DefaultHairFemale_01, PolygonSnow_SM_Attachment_DefaultHairMale_01, PolygonSnow_SM_Attachment_FaceMask_Female_01, PolygonSnow_SM_Attachment_FaceMask_Male_01, PolygonSnow_SM_Attachment_Goggles_01, PolygonSnow_SM_Attachment_Helmet_01, PolygonSnow_SM_Attachment_Helmet_02, PolygonSnow_SM_Attachment_Helmet_03, PolygonSnow_SM_Attachment_Helmet_Bike_01, PolygonSnow_SM_Attachment_Helmet_Bike_02,
        PolygonSpy_SM_Chr_Attach_Glasses_01, PolygonSpy_SM_Chr_Attach_NVG_Female_01, PolygonSpy_SM_Chr_Attach_NVG_Male_01, PolygonSpy_SM_Chr_Attach_Rose_01,
        // PolygonStarter
        // PolygonTown
        // PolygonVikings
        PolygonWar_SM_Char_Attach_American_Backpack_01, PolygonWar_SM_Char_Attach_American_Bedroll_01, PolygonWar_SM_Char_Attach_American_Canteen_01, PolygonWar_SM_Char_Attach_American_Helmet_01, PolygonWar_SM_Char_Attach_American_Helmet_02, PolygonWar_SM_Char_Attach_American_Helmet_03, PolygonWar_SM_Char_Attach_American_Pack_01, PolygonWar_SM_Char_Attach_American_Pouch_01, PolygonWar_SM_Char_Attach_American_Pouch_Front_01, PolygonWar_SM_Char_Attach_American_Pouch_Front_02, PolygonWar_SM_Char_Attach_American_Shovel_01, PolygonWar_SM_Char_Attach_American_TankHelmet_01, PolygonWar_SM_Char_Attach_Australian_Hat_01, PolygonWar_SM_Char_Attach_Bandage_Arm, PolygonWar_SM_Char_Attach_Bandage_Head_01, PolygonWar_SM_Char_Attach_Bandage_Head_02, PolygonWar_SM_Char_Attach_British_Beret_01, PolygonWar_SM_Char_Attach_British_Helmet_01, PolygonWar_SM_Char_Attach_Cigar_01, PolygonWar_SM_Char_Attach_Field_Glasses_01, PolygonWar_SM_Char_Attach_German_AmmoPack_01__1, PolygonWar_SM_Char_Attach_German_AmmoPack_01, PolygonWar_SM_Char_Attach_German_Bedroll_01, PolygonWar_SM_Char_Attach_German_Canister_01, PolygonWar_SM_Char_Attach_German_Canteen_01, PolygonWar_SM_Char_Attach_German_Hat_01, PolygonWar_SM_Char_Attach_German_Hat_02, PolygonWar_SM_Char_Attach_German_Helmet_01, PolygonWar_SM_Char_Attach_German_Helmet_02, PolygonWar_SM_Char_Attach_German_Officer_Hat_01, PolygonWar_SM_Char_Attach_German_Officer_Hat_02, PolygonWar_SM_Char_Attach_German_Pack_01, PolygonWar_SM_Char_Attach_German_Pouch_Front_01, PolygonWar_SM_Char_Attach_German_Shovel_01, PolygonWar_SM_Char_Attach_Glasses_01, PolygonWar_SM_Char_Attach_Hair_01, PolygonWar_SM_Char_Attach_Hair_02, PolygonWar_SM_Char_Attach_Hair_03, PolygonWar_SM_Char_Attach_Hair_Mustache_01, PolygonWar_SM_Char_Attach_Hair_Mustache_02, PolygonWar_SM_Char_Attach_Hair_Mustache_03, PolygonWar_SM_Char_Attach_Hair_Mustache_04, PolygonWar_SM_Char_Attach_Hair_Sideburns_01, PolygonWar_SM_Char_Attach_Mustache_01, PolygonWar_SM_Char_Attach_NewZealand_Hat_01, PolygonWar_SM_Char_Attach_Pilot_Helmet_01, PolygonWar_SM_Char_Attach_Pilot_Mask_01, PolygonWar_SM_Char_Attach_Russian_Hat_01, PolygonWar_SM_Char_Attach_Russian_Hat_02, PolygonWar_SM_Char_Attach_Russian_Hat_03, PolygonWar_SM_Char_Attach_TopHat_01,
        // PolygonWestern
        // PolygonWesternFrontier
        // PolygonZombies
    }
    public partial class Poly : Mb {
        public static PolyAtcData PadNone = new PolyAtcData(PolyBodyPart.None);
        public static PolyAtcData PadHips = new PolyAtcData(PolyBodyPart.Hips);
        public static PolyAtcData PadSpine1 = new PolyAtcData(PolyBodyPart.Spine_01);
        public static PolyAtcData PadSpine2 = new PolyAtcData(PolyBodyPart.Spine_02);
        public static PolyAtcData PadSpine3 = new PolyAtcData(PolyBodyPart.Spine_03);
        public static PolyAtcData PadClavicleL = new PolyAtcData(PolyBodyPart.Clavicle_L);
        public static PolyAtcData PadShoulderL = new PolyAtcData(PolyBodyPart.Shoulder_L);
        public static PolyAtcData PadElbowL = new PolyAtcData(PolyBodyPart.Elbow_L);
        public static PolyAtcData PadHandL = new PolyAtcData(PolyBodyPart.Hand_L);
        public static PolyAtcData PadClavicleR = new PolyAtcData(PolyBodyPart.Clavicle_R);
        public static PolyAtcData PadShoulderR = new PolyAtcData(PolyBodyPart.Shoulder_R);
        public static PolyAtcData PadElbowR = new PolyAtcData(PolyBodyPart.Elbow_R);
        public static PolyAtcData PadHandR = new PolyAtcData(PolyBodyPart.Hand_R);
        public static PolyAtcData PadHead = new PolyAtcData(PolyBodyPart.Head);
        public static PolyAtcData PadUpperLegL = new PolyAtcData(PolyBodyPart.UpperLeg_L);
        public static PolyAtcData PadLowerLegL = new PolyAtcData(PolyBodyPart.LowerLeg_L);
        public static PolyAtcData PadUpperLegR = new PolyAtcData(PolyBodyPart.UpperLeg_R);
        public static PolyAtcData PadLowerLegR = new PolyAtcData(PolyBodyPart.LowerLeg_R);
        public static Dictionary<PolyAtc, PolyAtcData> PolyAtcDic = new Dictionary<PolyAtc, PolyAtcData>() {
            // PolygonAdventure
            // PolygonApocalypse
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Knee_Metal_L_01,           null },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Knee_Metal_R_01,           null },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Knee_Sports_L_01,          PadLowerLegL.C.P(0, -0.008f, -0.004f).R(-105, 180, -90) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Knee_Sports_R_01,          PadLowerLegR.C.P(0, -0.007f, -0.006f).R(75, 180, 90) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Metal_L_01,       null },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Metal_L_02,       PadClavicleL.C.P(0.036f, 0.016f, -0.004f).R(0.3f, 28, -0.7f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Metal_R_01,       null },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Metal_R_02,       PadClavicleR.C.P(-0.006f, -0.003f, -0.018f).R(-179.7f, 28, 0.7f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Soft_L_01,        PadClavicleL.C.Ry(28) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Soft_R_01,        PadClavicleR.C.Rxy(-180, 30) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Sports_L_01,      PadClavicleL.C.P(-0.018f, 0.0171f, -0.005f).R(0.3f, 28, -0.7f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Sports_L_02,      PadClavicleL.C.P(-0.003f, 0.0159f, -0.0136f).R(0.3f, 28, -0.7f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Sports_R_01,      PadClavicleR.C.P(0.016f, -0.017f, 0.007f).R(-179.7f, 28, 0.7f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Shoulder_Sports_R_02,      PadClavicleR.C.P(0.03f, -0.006f, -0.022f).R(-179.7f, 28, 0.7f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Thigh_Metal_L_01,          PadUpperLegL.C.P(-0.098f, 0.008f, 0.0205f).Rz(91.8f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Thigh_Metal_R_01,          null },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Wrist_Metal_L_01,          null },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Armour_Wrist_Metal_R_01,          null },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Backpack_01,                      PadSpine3.C.Rxy(85.6f, -90) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Backpack_02,                      PadSpine3.C.Pxy(0.031f, -0.004f).Rxy(81.7f, -90) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Backpack_Large_01,                PadSpine3.C.P(0.077f, 0.006f, 0.014f).Rxy(85.6f, -90) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Backpack_Large_Extras_01,         PadSpine3.C.P(0.107f, 0.001f, 0.015f).Rxy(85.6f, -90) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Backpack_Small_01,                PadSpine3.C.P(0.009f, -0.027f, 0.005f).Rxy(85.6f, -90) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Bedroll_01,                       PadSpine3.C.Pxy(0.419f, 0.145f).Rxy(85.6f, -90) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Biker_Male_Beard_01,              PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Biker_Male_Hair_01,               PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Bullets_01,                       null },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Business_Male_Hair_01,            PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Canteen_01,                       PadHips.C.P(0.012f, -0.002f, 0.152f).R(-13, 2, 81.2f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Cool_Female_Hair_01,              PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Cool_Male_Hair_01,                PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Criminal_Male_Beard_01,           PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Criminal_Male_Hair_01,            PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Emo_Female_Hair_01,               PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Emo_Female_Spikes_L_01,           PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Emo_Female_Spikes_R_01,           PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_FootballHelmet_01,                PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_GasMask_01,                       PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Hazmat_Male_Glass_01,             PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Homeless_Male_Beard_01,           PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Hunter_Male_Beard_01,             PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Hunter_Male_Hat_01,               PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Hunter_Male_Hat_02,               PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Islander_Male_Hair_01,            PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Knife_01,                         PadHips.C.P(-0.025f, 0.017f, -0.193f).R(35.7f, -6.2f, 79.4f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Mags_01,                          PadHips.C.P(-0.109f, 0.024f, -0.146f).R(-11.6f, 192.8f, -77) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Mags_02,                          PadHips.C.P(-0.009f, 0.01f, -0.176f).R(-7.1f, 186.6f, -81.5f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Mags_03,                          PadHips.C.P(-0.023f, -0.001f, -0.178f).R(-7, 179, 81.9f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Mask_01,                          PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Mask_Hockey_01,                   PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Mechanic_Female_Hair_01,          PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Nerd_Female_Glasses_01,           PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Nerd_Female_Hair_01,              PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Nun_Female_HipPouch_01,           PadUpperLegR.C.Ryz(180, 90) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Patient_Female_Hair_01,           PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Pouch_01,                         PadHips.C.P(-0.114f, 0.016f, 0.166f).R(-5.4f, -5.6f, 81.9f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Pouch_02,                         PadHips.C.P(-0.115f, 0.014f, 0.184f).R(-7.9f, 1.1f, 74.6f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Pouch_03,                         PadHips.C.P(-0.062f, 0.022f, -0.16f).R(-4.2f, 186.1f, -70.8f) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Press_Male_Beard_01,              PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Press_Male_Eyepatch_01,           PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Press_Male_Hair_01,               PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Punk_Female_Hair_01,              PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_RiotCop_Male_Beard_01,            PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_RiotCop_Male_Hair_01,             PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_RiotCop_Male_Helmet_01,           PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_RiotCop_Male_Radio_01,            PadUpperLegR.C.Ryz(180, 90) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Scout_Female_Hat_01,              PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Scout_Female_Radio_01,            PadSpine2.C.Rxy(-90, 90) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Sheriff_Male_Hair_01,             PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Sheriff_Male_Hat_01,              PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Soldier_Female_Hair_01,           PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Soldier_Male_Glass_01,            PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Soldier_Male_Helmet_01,           PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_SupplyBag_01,                     PadHips.C.Pxy(-0.16f, -0.09f).Rxy(77.1f, -90) },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Teen_Female_Glasses_01,           PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Teen_Female_Hair_01,              PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Teen_Male_Hat_01,                 PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Waitress_Female_Glasses_01,       PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Waitress_Female_Hair_01,          PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Wanderer_Male_Beard_01,           PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Wanderer_Male_Hair_01,            PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Zombie_Female_Hair_01,            PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Zombie_Female_Hair_02,            PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Zombie_Male_Beard_01,             PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Zombie_Male_Hair_01,              PadHead.C },
            { PolyAtc.PolygonApocalypse_SM_Chr_Attach_Zombie_Male_Hair_02,              PadHead.C },
            // PolygonBattleRoyale
            { PolyAtc.PolygonBattleRoyale_SM_Char_Attach_Female_Armor_01,               PadNone.C.show },
            { PolyAtc.PolygonBattleRoyale_SM_Char_Attach_Female_Armor_02,               PadNone.C.show },
            { PolyAtc.PolygonBattleRoyale_SM_Char_Attach_Female_Armor_03,               PadNone.C.show },
            { PolyAtc.PolygonBattleRoyale_SM_Char_Attach_Male_Armor_01,                 PadNone.C.show },
            { PolyAtc.PolygonBattleRoyale_SM_Char_Attach_Male_Armor_02,                 PadNone.C.show },
            { PolyAtc.PolygonBattleRoyale_SM_Char_Attach_Male_Armor_03,                 PadNone.C.show },
            { PolyAtc.PolygonBattleRoyale_SM_Char_Attach_Bag_Lvl1,                      PadSpine2.C.Pxy(-0.2f, -0.05f).Rxy(-90, 90) },
            { PolyAtc.PolygonBattleRoyale_SM_Char_Attach_Bag_Lvl2,                      PadSpine2.C.Pxy(-0.2f, -0.05f).Rxy(-90, 90) },
            { PolyAtc.PolygonBattleRoyale_SM_Char_Attach_Bag_Lvl3,                      PadSpine2.C.Pxy(-0.2f, -0.05f).Rxy(-90, 90) },
            { PolyAtc.PolygonBattleRoyale_SM_Char_Attach_Helmet_01,                     PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Char_Attach_Helmet_02,                     PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Char_Attach_Helmet_03,                     PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Ammo_01,                        PadHips.C.Pyz(0.1f, 0.1f).Rxy(60, 180) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Beard_02,                       PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Beard_03,                       PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Beard_04,                       PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Beard_05,                       PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Beard_06,                       PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Beard_07,                       PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Bedroll_01,                     PadSpine2.C.Pxy(0.18f, -0.22f).Rxy(-90, 90) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Earmuffs_01,                    PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Eyepatch_01,                    PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Facemask_01,                    PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Facemask_02,                    PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Facemask_03,                    PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Female_01,                      PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Female_02,                      PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Female_Default_Hair_01,         PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Female_Hair_01,                 PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Female_Hair_Pigtails_01,        PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_GasMask_01,                     PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Glasses_01,                     PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Bandana_01,                 PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Beanie_01,                  PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Beer_01,                    PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Bunny_01,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Captian_01,                 PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Cap_01,                     PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Cowboy_01,                  PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Crown_01,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Cupcake_01,                 PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Elf_01,                     PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Fez_01,                     PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_FlatTop_01,                 PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Football_01,                PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_IceCream_01,                PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Irish_01,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Knight_01,                  PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Mexican_01,                 PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Mil_01,                     PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Party_01,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Pot_01,                     PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Racoon_01,                  PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Roadcone_01,                PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Roman_01,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Samurai_01,                 PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Shark_01,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Squid_01,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Tacticle_01,                PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Turban_01,                  PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Hat_Unicorn_01,                 PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Male_Default_Hair_01,           PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_01,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_02,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_03,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_04,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_05,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_06,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Male_Hair_07,                   PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Pouch_01,                       PadHips.C.Pyz(-0.09f, -0.2f).R(20, 180, -80) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Pouch_02,                       PadHips.C.Pyz(0.01f, -0.21f).R(-10, 180, -80) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Pouch_03,                       PadHips.C.Pyz(0.12f, -0.16f).R(-60, 180, -85) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Pouch_04,                       PadHips.C.Pyz(0.15f, -0.07f).R(-80, 180, -85) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Pouch_05,                       PadHips.C.Pyz(0.16f, 0.02f).R(-90, 180, -90) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Pouch_06,                       PadHips.C.Pyz(0.14f, 0.12f).R(-115, 180, -90) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Pouch_07,                       PadHips.C.Pyz(-0.1f, 0.2f).R(-170, 180, -100) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Pouch_08,                       PadHips.C.Pyz(0.08f, 0.2f).R(-150, 180, -100) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Scarf_01,                       PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Attach_Scarf_02,                       PadHead.C },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Patch_Flag_01,                         PadShoulderR.C.Pxy(0.05f, -0.05f).Rxz(90, 90) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Patch_Leaf_01,                         PadShoulderR.C.Pxy(0.05f, -0.05f).Rxz(90, 90) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Patch_Medic_01,                        PadShoulderR.C.Pxy(0.05f, -0.05f).Rxz(90, 90) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Patch_Medic_02,                        PadShoulderR.C.Pxy(0.05f, -0.05f).Rxz(90, 90) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Patch_Rank_01,                         PadShoulderR.C.Pxy(0.05f, -0.05f).Rxz(90, 90) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Patch_Rank_02,                         PadShoulderR.C.Pxy(0.05f, -0.05f).Rxz(90, 90) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Patch_Rank_03,                         PadShoulderR.C.Pxy(0.05f, -0.05f).Rxz(90, 90) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Patch_Star_01,                         PadShoulderR.C.Pxy(0.05f, -0.05f).Rxz(90, 90) },
            { PolyAtc.PolygonBattleRoyale_SM_Chr_Patch_Star_02,                         PadShoulderR.C.Pxy(0.05f, -0.05f).Rxz(90, 90) },
            // PolygonBossZombies
            { PolyAtc.PolygonBossZombies_SM_Prop_Cigarette_01,                          PadHead.C.Pz(0.08f).Rx(100) },
            { PolyAtc.PolygonBossZombies_SM_Prop_HeartGlasses_01,                       PadHead.C.Pyz(0.1f, 0.04f) },
            { PolyAtc.PolygonBossZombies_SM_Prop_Ribbon_01,                             PadSpine2.C.P(0.04f, 0.3f, 0.2f).Rxz(-90, 90) },
            { PolyAtc.PolygonBossZombies_SM_Chr_Attach_Blobber_Hair_01,                 PadHead.C },
            { PolyAtc.PolygonBossZombies_SM_Chr_Attach_Blobber_Hair_02,                 PadHead.C },
            { PolyAtc.PolygonBossZombies_SM_Chr_Attach_Blobber_Tiara_01,                PadHead.C },
            { PolyAtc.PolygonBossZombies_SM_Chr_Attach_Blobber_Tiara_02,                PadHead.C },
            { PolyAtc.PolygonBossZombies_SM_Chr_Attach_Brute_Bandana_01,                PadHead.C },
            { PolyAtc.PolygonBossZombies_SM_Chr_Attach_Brute_Extra_01,                  PadHead.C },
            { PolyAtc.PolygonBossZombies_SM_Chr_Attach_Brute_Hair_01,                   PadHead.C },
            { PolyAtc.PolygonBossZombies_SM_Chr_Attach_Brute_Hair_02,                   PadHead.C },
            { PolyAtc.PolygonBossZombies_SM_Chr_Attach_Slobber_FacialHair_01,           PadHead.C },
            { PolyAtc.PolygonBossZombies_SM_Chr_Attach_Slobber_Hair_01,                 PadHead.C },
            { PolyAtc.PolygonBossZombies_SM_Chr_Attach_Wretch_Hair_01,                  PadHead.C },
            { PolyAtc.PolygonBossZombies_SM_Chr_Attach_Wretch_Hair_02,                  PadHead.C },
            // PolygonCity
            // POLYGONCityCharacters
            // PolygonConstruction
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Beard_01,                       PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Beard_02,                       PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Beard_03,                       PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Beard_Grey_01,                  PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Beard_Grey_02,                  PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_BeltLoop_01,                    PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_BeltLoop_Chisel_01,             PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_BeltLoop_Hammer_01,             PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_BeltLoop_Screwdriver_01,        PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Cigarette_01,                   PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Cigarette_02,                   PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Earmuffs_01,                    PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_FaceShield_Mesh_01,             PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_FaceShield_Plastic_01,          PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_FilterMask_01,                  PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Goatee_01,                      PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Goggles_01,                     PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Goggles_OnHead_01,              PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Hat_01,                         PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Helmet_01,                      PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Helmet_02,                      PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Helmet_Backwards_01,            PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Helmet_Earmuffs_01,             PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Helmet_Earmuffs_02,             PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Helmet_Goggles_01,              PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Helmet_Hat_01,                  PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_ID_01,                          PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Mustache_01,                    PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Pouch_01,                       PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_Pouch_02,                       PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_SafteyGlasses_01,               PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_SunGlasses_01,                  PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Attach_WelderMask_01,                  PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Inspector_Female_Glasses_01,           PadHead.C },
            { PolyAtc.PolygonConstruction_SM_Chr_Inspector_Male_Glasses_01,             PadHead.C },
            // PolygonDungeon
            // PolygonExplorers
            { PolyAtc.PolygonExplorers_SM_Chr_Attach_Backpack_01,                       PadHead.C.Pxy(-0.15f, -0.1f).Rxy(-90, 90) },
            { PolyAtc.PolygonExplorers_SM_Chr_Attach_Beard_01,                          PadHead.C },
            { PolyAtc.PolygonExplorers_SM_Chr_Attach_Hair_Female_01,                    PadHead.C },
            { PolyAtc.PolygonExplorers_SM_Chr_Attach_Hair_Female_Hat_01,                PadHead.C },
            { PolyAtc.PolygonExplorers_SM_Chr_Attach_Hair_Male_01,                      PadHead.C },
            { PolyAtc.PolygonExplorers_SM_Chr_Attach_Hair_Male_Hat_01,                  PadHead.C },
            { PolyAtc.PolygonExplorers_SM_Chr_Attach_Hat_Female_01,                     PadHead.C },
            { PolyAtc.PolygonExplorers_SM_Chr_Attach_Hat_Hunter_01,                     PadHead.C },
            { PolyAtc.PolygonExplorers_SM_Chr_Attach_Hat_Male_01,                       PadHead.C },
            { PolyAtc.PolygonExplorers_SM_Chr_Attach_Pouch_01,                          PadHead.C.Pyz(-0.11f, 0.11f).Rxz(135, 90) },
            { PolyAtc.PolygonExplorers_SM_Chr_Attach_Rope_01,                           PadHead.C.Pz(0.17f).Rz(90) },
            { PolyAtc.PolygonExplorers_SM_Chr_Attach_Whip_01,                           PadHead.C.Pz(-0.18f).Rxy(90, -90) },
            // PolygonFantasyCharacters
            // PolygonFantasyHeroCharacters
            // PolygonFantasyKingdom
            { PolyAtc.PolygonFantasyKingdom_SM_Chr_Attach_Blacksmith_Female_Goggles_02, PadHead.C.Pyz(0.053f, 0.022f) },
            { PolyAtc.PolygonFantasyKingdom_SM_Chr_Attach_Blacksmith_Male_Goggles_01,   PadHead.C.Pyz(0.067f, 0.021f) },
            { PolyAtc.PolygonFantasyKingdom_SM_Chr_Attach_Blacksmith_Male_Goggles_02,   PadHead.C.Pyz(0.067f, 0.021f) },
            { PolyAtc.PolygonFantasyKingdom_SM_Chr_Attach_King_Cape_01,                 PadSpine3.C.Pxy(-0.003f, 0.033f).Rxy(85.6f, -90) },
            { PolyAtc.PolygonFantasyKingdom_SM_Chr_Attach_King_Crown_01,                PadHead.C.Pyz(0.051f, 0.009f) },
            { PolyAtc.PolygonFantasyKingdom_SM_Chr_Attach_Mage_Cape_01,                 PadSpine3.C.Pxy(-0.016f, 0.001f).Rxy(90, -90) },
            { PolyAtc.PolygonFantasyKingdom_SM_Chr_Attach_Priest_Hat_01,                PadHead.C.Pyz(0.062f, 0.025f) },
            { PolyAtc.PolygonFantasyKingdom_SM_Chr_Attach_Princess_Tiara_01,            PadHead.C.Pyz(0.057f, 0.03f) },
            { PolyAtc.PolygonFantasyKingdom_SM_Chr_Attach_Queen_Crown_01,               PadHead.C.Pyz(0.068f, 0.024f) },
            { PolyAtc.PolygonFantasyKingdom_SM_Chr_Attach_Soldier_01,                   PadHead.C.Pyz(0.062f, 0.022f) },
            // PolygonFantasyRivals
            // PolygonFarm
            { PolyAtc.PolygonFarm_SM_Chr_Attach_Buckethat_01___lp_1_rp_,                PadHead.C },
            { PolyAtc.PolygonFarm_SM_Chr_Attach_Cap_01,                                 PadHead.C },
            { PolyAtc.PolygonFarm_SM_Chr_Attach_Cap_02,                                 PadHead.C },
            { PolyAtc.PolygonFarm_SM_Chr_Attach_CowboyHat_01,                           PadHead.C },
            { PolyAtc.PolygonFarm_SM_Chr_Attach_CowboyHat_02,                           PadHead.C },
            { PolyAtc.PolygonFarm_SM_Chr_Attach_Glasses_01,                             PadHead.C },
            { PolyAtc.PolygonFarm_SM_Chr_Attach_ScarecrowHat_01,                        PadHead.C },
            { PolyAtc.PolygonFarm_SM_Chr_Attach_Wheat_01,                               PadHead.C },
            // PolygonGangWarfare
            // PolygonHeist
            { PolyAtc.PolygonHeist_SM_Item_Hair_Eyebrow_Man_01,                         PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Hair_Eyebrow_Woman_01,                       PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Hair_Man_01,                                 PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Hair_Woman_01,                               PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Alien_01,                               PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Balaclava_01,                           PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Beanie_01,                              PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Chicken_01,                             PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Clown_01,                               PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Construction_01,                        PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Fox_01,                                 PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Gas_01,                                 PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Glasses_01,                             PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Hat_01,                                 PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Hockey_01,                              PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Horse_01,                               PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Luchador_01,                            PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Panda_01,                               PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_PaperBag_01,                            PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Simple_Man_01,                          PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Simple_Robber_01,                       PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Simple_Woman_01,                        PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Tiger_01,                               PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_Trump_01,                               PadHead.C },
            { PolyAtc.PolygonHeist_SM_Item_Mask_WeldersMask_01,                         PadHead.C },
            // PolygonKnights
            // PolygonMilitary
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Nameplate_01,                       PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Nameplate_02,                       PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Nameplate_03,                       PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Nameplate_04,                       PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Nameplate_05,                       PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Nameplate_06,                       PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Nameplate_07,                       PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Nameplate_08,                       PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Nameplate_09,                       PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Nameplate_10,                       PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Flag_01,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Flag_02,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Flag_03,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Flag_04,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Flag_05,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Flag_06,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Flag_07,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Flag_08,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Flag_09,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Flag_10,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Flag_11,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Flag_12,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Rank_01,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Rank_02,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Rank_03,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Rank_04,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Rank_05,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Rank_06,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Rank_07,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Rank_08,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Rank_09,                      PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Squad_01,                     PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Squad_02,                     PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Squad_03,                     PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Squad_04,                     PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Squad_05,                     PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Squad_06,                     PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Squad_07,                     PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Squad_08,                     PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Patch_Squad_09,                     PadShoulderR.C.P(0.081f, -0.04f, -0.02f).R(-56.2f, -25.1f, -242.2f) },
            { PolyAtc.PolygonMilitary_SM_Char_Attach_Beret_01,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Char_Attach_Pilot_Helmet_01,                   PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Backpack_01,                        PadSpine2.C.Pxy(-0.042f, -0.211f).Rxy(83.4f, -90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Backpack_02,                        PadSpine2.C.Pxy(-0.132f, -0.204f).R(95.4f, -271.6f, -181.2f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Battery_01,                         PadSpine2.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beanie_01,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beard_01,                           PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beard_02,                           PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beard_03,                           PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beard_04,                           PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beard_05,                           PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beard_06,                           PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beard_07,                           PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beard_08,                           PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beard_09,                           PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beard_10,                           PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beard_11,                           PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Beard_12,                           PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Bombsuit_Helmet_01,                 PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Bombsuit_Wrist_01,                  PadElbowL.C.P(-0.162f, 0.078f, -0.012f).Rxz(-90, -94.1f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Bomb_C4_01,                         PadSpine1.C.P(-0.081f, -0.017f, 0.176f).R(-179.5f, 3.3f, 90.4f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Bomb_C4_02,                         PadSpine1.C.P(-0.064f, -0.035f, -0.199f).Rz(108.3f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Bomb_Dynamite_01,                   null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Bomb_Dynamite_02,                   null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Bomb_Kit_01,                        PadSpine1.C.Pxy(0.009f, 0.167f).Rxy(-102.4f, 90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Book_Pouch_01,                      null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Book_Pouch_02,                      null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Chin_Strap_01_Female,               PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Chin_Strap_01_Male,                 PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Chin_Strap_02_Female,               PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Chin_Strap_02_Male,                 PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_CowboyHat_01,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Earmuffs_01,                        PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Eyepatch_01,                        PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Gas_Mask_01,                        PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Gas_Mask_01_Strap,                  PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Glasses_01,                         PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Glasses_02,                         PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Glasses_03,                         PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Glowstick_01,                       PadSpine1.C.P(-0.108f, -0.008f, 0.18f).Rz(90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Goggles_01,                         PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Goggles_Strap_01,                   PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Grenade_01,                         null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Grenade_Flash_01,                   PadSpine3.C.P(-0.065f, -0.131f, -0.137f).R(39.7f, -94.9f, -12.9f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Grenade_Smoke_01,                   null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Female_01,                     PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Female_02,                     PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Female_03,                     PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Female_04,                     PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Female_05,                     PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Male_01,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Male_02,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Male_03,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Male_04,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Male_05,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Male_06,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Male_07,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Male_08,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Male_09,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Male_10,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hair_Male_11,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hat_01,                             PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hat_02,                             PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hat_03,                             PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hat_04,                             PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hat_05,                             PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hat_06,                             PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hat_07,                             PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hat_Cap_01,                         PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Hat_Cap_02,                         PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Headset_01,                         PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_01,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_01_Goggles_01,               PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_01_Goggles_02,               PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_01_Goggles_03,               PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_02,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_03,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_04,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_05,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_06,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_07,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_08,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_09,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_10,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_11_Female,                   PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Helmet_11_Male,                     PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Holster_01,                         null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Holster_Pistol_01,                  null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Holster_Pistol_02,                  PadUpperLegR.C.P(-0.013f, 0.072f, -0.042f).R(-105.9f, 20.1f, -104.8f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Holster_Pistol_03,                  PadUpperLegR.C.P(0.004f, 0.077f, -0.016f).R(-94.8f, 44.2f, -139) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Knife_Sheath_01,                    PadUpperLegL.C.P(0.231f, -0.105f, -0.016f).R(84.2f, 90, 180) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Mustache_01,                        PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_NVG_01,                             PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_NVG_02,                             PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_NVG_03,                             PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_NVG_Mount_01,                       PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Padding_01,                         null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_01,                           PadSpine1.C.P(-0.1f, -0.023f, -0.175f).Rxz(-180, -90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_02,                           PadSpine1.C.P(-0.088f, -0.021f, 0.209f).Rz(90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_03,                           PadSpine1.C.Pz(-0.212f).Ryz(-180, -90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_04,                           PadUpperLegL.C.P(0.254f, -0.097f, -0.005f).R(95.1f, -30.2f, 59.9f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_05,                           PadHips.C.P(0.038f, 0.133f, -0.057f).R(-69.7f, 133.5f, -41.7f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_06,                           PadSpine1.C.P(-0.035f, 0.009f, 0.218f).Rz(90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_07,                           PadSpine1.C.P(-0.109f, -0.109f, 0.14f).Rxz(45, 90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_08,                           PadSpine1.C.Pxy(-0.015f, -0.136f).Rxy(87, 270) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_09,                           PadSpine1.C.P(-0.058f, -0.016f, 0.247f).Rz(90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_Grenade_01,                   PadSpine1.C.P(-0.099f, -0.091f, -0.125f).R(45, -180, -90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_Large_01,                     null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_Mag_01,                       null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_Mag_02,                       null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Double_01,                PadSpine1.C.P(-0.119f, -0.189f, 0.0486f).R(94.7f, -180, 90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Double_02,                null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Pistol_01,                PadSpine1.C.P(-0.102f, -0.013f, 0.185f).R(72.8f, 9.5f, 90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Single_01,                PadSpine1.C.P(-0.163f, -0.151f, 0.049f).R(-261.9f, -152.7f, -62.4f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Single_02,                PadSpine1.C.P(-0.146f, -0.153f, 0.057f).R(78, 24.7f, 111.8f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Single_Handle_01,         null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_Mag_Single_Handle_02,         null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_WalkieTalkie_01,              PadClavicleR.C.P(0.055f, -0.008f, -0.079f).R(19.7f, -166, -210.3f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Pouch_WalkieTalkie_Empty_01,        null },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Radio_01,                           PadSpine3.C.P(-0.06f, -0.112f, -0.137f).R(71.5f, -68.7f, 35.4f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_ShellStrip_Rifle_01,                PadSpine2.C.P(-0.043f, 0.179f, 0.033f).R(-74.1f, 70.2f, 109.1f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_ShellStrip_Shotgun_01,              PadClavicleL.C.P(-0.045f, -0.001f, 0.084f).R(-38.3f, 14.5f, 10.9f) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_SunGlasses_01_Female,               PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_SunGlasses_01_Male,                 PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Tool_01,                            PadSpine2.C.P(-0.071f, 0.193f, -0.029f).R(-105, -90, -90) },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Tool_02,                            PadSpine2.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Torch_01,                           PadSpine2.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Turban_01,                          PadHead.C },
            { PolyAtc.PolygonMilitary_SM_Chr_Attach_Turban_02,                          PadHead.C },
            // PolygonOffice
            { PolyAtc.PolygonOffice_SM_Chr_Attachment_Antlers_01,                       PadHead.C },
            { PolyAtc.PolygonOffice_SM_Chr_Attachment_Headset_01,                       PadHead.C },
            { PolyAtc.PolygonOffice_SM_Chr_Attachment_Headset_02,                       PadHead.C },
            { PolyAtc.PolygonOffice_SM_Chr_Attachment_Identity_01,                      PadSpine3.C.P(0.03f, -0.15f, -0.1f).Rxy(100, -90) },
            { PolyAtc.PolygonOffice_SM_Chr_Attachment_Nametag_01,                       PadSpine3.C.P(0.03f, -0.15f, -0.1f).Rxy(100, -90) },
            { PolyAtc.PolygonOffice_SM_Chr_Attachment_Pens_01,                          PadSpine3.C.P(0.03f, -0.15f, -0.1f).Rxy(100, -90) },
            // PolygonPirates
            // PolygonPrototype
            // PolygonSamurai
            // PolygonSciFiCity
            // PolygonSciFiSpace
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_SpaceHelmet_01,                   PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Crew_Female_Hair_01,              PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Crew_Male_Hair_01,                PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_EVA_Cover_01,                     PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_EVA_Cover_Clear_01,               PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Hat_Captain_01,                   PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Hat_Crew_01,                      PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Hunter_Female_Hair_01,            PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Junker_Hair_Female_01,            PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Junker_Headset_01,                PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Junker_Helmet_01,                 PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Junker_Helmet_Glass_01,           PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Male_Hair_01,                     PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Male_Hair_02,                     PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Male_Hair_03,                     PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Male_Hair_05,                     PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Male_Hair_06,                     PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Male_Hair_07,                     PadHead.C },
            { PolyAtc.PolygonSciFiSpace_SM_Chr_Attach_Mask_Medic_01,                    PadHead.C },
            // PolygonSnow
            { PolyAtc.PolygonSnow_SM_Attachment_Balaclava_01,                           PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_Beanie_01,                              PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_Beanie_02,                              PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_Beanie_03,                              PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_DefaultHairFemale_01,                   PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_DefaultHairMale_01,                     PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_FaceMask_Female_01,                     PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_FaceMask_Male_01,                       PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_Goggles_01,                             PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_Helmet_01,                              PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_Helmet_02,                              PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_Helmet_03,                              PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_Helmet_Bike_01,                         PadHead.C },
            { PolyAtc.PolygonSnow_SM_Attachment_Helmet_Bike_02,                         PadHead.C },
            // PolygonSpy
            { PolyAtc.PolygonSpy_SM_Chr_Attach_Glasses_01,                              PadHead.C },
            { PolyAtc.PolygonSpy_SM_Chr_Attach_NVG_Female_01,                           PadHead.C },
            { PolyAtc.PolygonSpy_SM_Chr_Attach_NVG_Male_01,                             PadHead.C },
            { PolyAtc.PolygonSpy_SM_Chr_Attach_Rose_01,                                 PadHead.C.P(0.1f, 0.16f, 0.04f).Rxz(20, 50) },
            // PolygonStarter
            // PolygonTown
            // PolygonVikings
            // PolygonWar
            { PolyAtc.PolygonWar_SM_Char_Attach_American_Backpack_01,                   PadSpine2.C.Pxy(0.016f, 0.014f).Rxy(-87.4f, 90) },
            { PolyAtc.PolygonWar_SM_Char_Attach_American_Bedroll_01,                    PadSpine2.C.Pxy(0.105f, -0.179f).Rxy(-81.6f, 90) },
            { PolyAtc.PolygonWar_SM_Char_Attach_American_Canteen_01,                    PadHips.C.P(-0.046f, 0.036f, -0.19f).R(-17.3f, 177, -76.9f) },
            { PolyAtc.PolygonWar_SM_Char_Attach_American_Helmet_01,                     PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_American_Helmet_02,                     PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_American_Helmet_03,                     PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_American_Pack_01,                       PadSpine2.C.Pxy(-0.173f, -0.177f).Rxy(85.1f, 270) },
            { PolyAtc.PolygonWar_SM_Char_Attach_American_Pouch_01,                      PadHips.C.P(0.035f, 0.137f, -0.093f).R(-112.1f, -21.6f, 115.1f) },
            { PolyAtc.PolygonWar_SM_Char_Attach_American_Pouch_Front_01,                PadHips.C.Pxy(-0.099f, 0.011f).Rxy(-81.4f, 90) },
            { PolyAtc.PolygonWar_SM_Char_Attach_American_Pouch_Front_02,                PadHips.C.Pxy(-0.109f, -0.01f).Rxy(-81.4f, 90) },
            { PolyAtc.PolygonWar_SM_Char_Attach_American_Shovel_01,                     PadSpine2.C.Pxy(0.106f, -0.301f).Rxy(81.6f, 270) },
            { PolyAtc.PolygonWar_SM_Char_Attach_American_TankHelmet_01,                 PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Australian_Hat_01,                      PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Bandage_Arm,                            PadShoulderL.C.P(-0.011f, 0.013f, 0.004f) },
            { PolyAtc.PolygonWar_SM_Char_Attach_Bandage_Head_01,                        PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Bandage_Head_02,                        PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_British_Beret_01,                       PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_British_Helmet_01,                      PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Cigar_01,                               PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Field_Glasses_01,                       PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_AmmoPack_01__1,                  PadHips.C.P(0.114f, 0.162f, -0.107f).R(-81.1f, -24.6f, 107.1f) },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_AmmoPack_01,                     PadHips.C.P(0.114f, 0.162f, -0.107f).R(-81.1f, -24.6f, 107.1f) },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_Bedroll_01,                      PadSpine2.C.Pxy(-0.024f, -0.159f).Rxy(-81.6f, 90) },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_Canister_01,                     PadHips.C.P(-0.154f, -0.087f, -0.119f).R(-59.1f, 33.4f, -0.4f) },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_Canteen_01,                      PadHips.C.Pxz(-0.074f, -0.234f).R(-2.9f, 192, -81.8f) },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_Hat_01,                          PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_Hat_02,                          PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_Helmet_01,                       PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_Helmet_02,                       PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_Officer_Hat_01,                  PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_Officer_Hat_02,                  PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_Pack_01,                         PadSpine2.C.Pxy(-0.201f, -0.19f).Rxy(89.5f, 90) },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_Pouch_Front_01,                  PadHips.C.Pxy(-0.098f, 0.003f).Rxy(-81.4f, 90) },
            { PolyAtc.PolygonWar_SM_Char_Attach_German_Shovel_01,                       PadHips.C.P(0.206f, -0.198f, 0.135f).R(66.5f, 25.5f, 55.3f) },
            { PolyAtc.PolygonWar_SM_Char_Attach_Glasses_01,                             PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Hair_01,                                PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Hair_02,                                PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Hair_03,                                PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Hair_Mustache_01,                       PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Hair_Mustache_02,                       PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Hair_Mustache_03,                       PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Hair_Mustache_04,                       PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Hair_Sideburns_01,                      PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Mustache_01,                            PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_NewZealand_Hat_01,                      PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Pilot_Helmet_01,                        PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Pilot_Mask_01,                          PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Russian_Hat_01,                         PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Russian_Hat_02,                         PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_Russian_Hat_03,                         PadHead.C },
            { PolyAtc.PolygonWar_SM_Char_Attach_TopHat_01,                              PadHead.C },
            // PolygonWestern
            // PolygonWesternFrontier
            // PolygonZombies
        };
        public static void CrtAtcs(GameObject chrGo, params PolyAtc[] atcs) {
            float scl = chrGo.PolyScl();
            for (int i = 0; i < atcs.Length; i++) {
                PolyAtcData data = PolyAtcDic[atcs[i]];
                if (data.NotNull()) {
                    PolyName pn = PolyName.Crt(atcs[i]);
                    if (data.isCrt) {
                        for (int j = 0; j < PathDic[pn.pack].chrAtcs.Count; j++) {
                            GameObject pf = A.LoadGo((pn.pack + "/" + PathDic[pn.pack].pf + "/" + PathDic[pn.pack].chrAtcs[j] + "/" + pn.name).Replace("//", "/"));
                            if (pf) {
                                Transform partTf = chrGo.PolyBodyPart(data.part);
                                GameObject go = Ins(pf, partTf.Tp(), partTf.Tr(), partTf);
                                go.PolyTf(data.tf.p * scl, data.tf.r, data.tf.s * scl);
                                break;
                            }
                        }
                    } else {
                        if (data.part == PolyBodyPart.None) {
                            chrGo.ChildNameShow(pn.name);
                        } else {
                            chrGo.PolyBodyPart(data.part).ChildNameShow(pn.name);
                        }
                    }
                }
            }
        }
    }
}