﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

namespace Polygon {
    public static class PolyExtension {
        public static Transform PolyBodyPart(this GameObject a, PolyBodyPart part) {
            List<string> paths = Poly.PartPathDic[part];
            for (int i = 0; i < paths.Count; i++) {
                Transform tf = a.ChildName(paths[i].Split('/'));
                if (tf)
                    return tf;
            }
            return a.transform;
        }
        public static Transform PolyRoot(this GameObject a) { return a.PolyBodyPart(Polygon.PolyBodyPart.Root); }
        public static Transform PolyHips(this GameObject a) { return a.PolyBodyPart(Polygon.PolyBodyPart.Hips); }
        public static Transform PolySpine1(this GameObject a) { return a.PolyBodyPart(Polygon.PolyBodyPart.Spine_01); }
        public static Transform PolySpine2(this GameObject a) { return a.PolyBodyPart(Polygon.PolyBodyPart.Spine_02); }
        public static Transform PolySpine3(this GameObject a) { return a.PolyBodyPart(Polygon.PolyBodyPart.Spine_03); }
        public static Transform PolyElbowL(this GameObject a) { return a.PolyBodyPart(Polygon.PolyBodyPart.Elbow_L); }
        public static Transform PolyHandL(this GameObject a) { return a.PolyBodyPart(Polygon.PolyBodyPart.Hand_L); }
        public static Transform PolyElbowR(this GameObject a) { return a.PolyBodyPart(Polygon.PolyBodyPart.Elbow_R); }
        public static Transform PolyHandR(this GameObject a) { return a.PolyBodyPart(Polygon.PolyBodyPart.Hand_R); }
        public static Transform PolyHead(this GameObject a) { return a.PolyBodyPart(Polygon.PolyBodyPart.Head); }
        public static float PolyScl(this GameObject a) { return 1f / a.PolyRoot().Tls().x; }
        public static void RotChr(this Animator a, Anim anim) { a.gameObject.TleY(Poly.AnimDic[anim].chrRotY); }
        public static void RotWep(this Animator a, Anim anim, GameObject wepGo) { wepGo.Tle(Poly.AnimDic[anim].wepRot); }
        public static void RotChrWep(this Animator a, Anim anim, GameObject wepGo) { a.RotChr(anim); a.RotWep(anim, wepGo); }
        public static void Anim(this Animator a, Anim anim) { a.Play(anim.tS()); }
        public static void AnimChr(this Animator a, Anim anim) { a.RotChr(anim); a.Anim(anim); }
        public static void AnimWep(this Animator a, Anim anim, GameObject wepGo) { a.RotWep(anim, wepGo); a.Anim(anim); }
        public static void AnimChrWep(this Animator a, Anim anim, GameObject wepGo) { a.RotChrWep(anim, wepGo); a.Anim(anim); }
        public static void AnimChrWep(this Animator a, Anim anim, GameObject wepGo, bool isChr, bool isWep) {
            if (isChr)
                a.RotChr(anim);
            if (isWep)
                a.RotWep(anim, wepGo);
            a.Anim(anim);
        }
        public static void PolyTf(this GameObject a, Vector3 p, Vector3 r) { a.Tlp(p); a.Tle(r); }
        public static void PolyTf(this GameObject a, Vector3 p, Vector3 r, Vector3 s) { a.Tlp(p); a.Tle(r); a.Tls(s); }
        public static void PolyTf(this GameObject a, Vector3 p, Vector3 r, float s) { a.Tlp(p * s); a.Tle(r); a.Tls(V3.V(s)); }
        public static void PsStop(this GameObject a) {
            a.PsMainMaxP(0);
            for (int i = 0, n = a.Tcc(); i < n; i++) {
                a.ChildGo(i).PsMainLoop(false);
                a.ChildGo(i).PsMainMaxP(0);
            }
        }
        public static void PsPrewarm(this GameObject a, bool isPrewarm) {
            a.PsMainPrewarm(isPrewarm);
            a.Hide();
            a.Show();
        }
        public static void PsCol(this GameObject a, TreeData<string> datas) { datas.Do(a, PsCols); }
        public static void PsCol(this GameObject a, PolyFX fx) { Poly.PolyFXDic[PolyFX.PolygonParticles_FX_GlowSpot_01].datas.Do(a, PsCols); }
        static void PsCols(GameObject go, TreeData<string> datas, int[] childs) {
            GameObject parGo = go.ChildGo(childs);
            if (datas.parent != "")
                parGo.PsMainStaC(Ps.Mmg(datas.parent.Hex()));
            for (int i = 0; i < datas.childs.Count; i++)
                if (datas.childs[i] != "")
                    parGo.ChildGo(i).PsMainStaC(Ps.Mmg(datas.childs[i].Hex()));
        }
    }
}