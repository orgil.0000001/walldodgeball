using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Ball : Mb {
    public bool isPlayer;
    public bool isInit = false;
    public float Spd => Ls.I.BallSpd(isPlayer);
    //public List<int> angs = new List<int>();
    //int angIdx;
    void Start() {
    }
    void Update() {
        if (IsPlaying) {
            if (!isInit && (isPlayer ? Tp.z > Ls.I.ballLim : Tp.z < -Ls.I.ballLim)) {
                go.layer = Lyr.Ball;
                isInit = true;
            }
            Tls = Ls.I.Zoom(isPlayer);
            rb.V(rb.velocity.normalized * Spd);
        }
    }
    public void Init() {
        Te = V3.Y(Te.y < 90 ? M.C(Te.y, 0, 90 - Ls.I.ballMinRot) : Te.y < 270 ? M.C(Te.y, 90 + Ls.I.ballMinRot, 270 - Ls.I.ballMinRot) : M.C(Te.y, 270 + Ls.I.ballMinRot, 360));
        rb.V(F * Spd);
        //Angs();
    }
    //public void Angs() {
    //    int ang = M.RoundI(Te.y);
    //    if (ang < 90) {
    //        angIdx = 0;
    //        angs = List(ang, 180 - ang, 180 + ang, -ang);
    //    } else if (ang < 180) {
    //        angIdx = 1;
    //        angs = List(180 - ang, ang, -ang, 180 + ang);
    //    } else if (ang < 270) {
    //        angIdx = 2;
    //        angs = List(180 + ang, -ang, ang, 180 - ang);
    //    } else {
    //        angIdx = 3;
    //        angs = List(-ang, 180 + ang, 180 - ang, ang);
    //    }
    //}
    //private void OnCollisionEnter(Collision collision) {
    //    if (collision.Tag(Tag.Wall)) {
    //        Vector3 p = collision.Tp(), sz = collision.Ts();
    //        print(sz);
    //        int dir = M.IsBet(Tp.x, p.x - sz.x, p.x + sz.x) ? 0 : M.IsBet(Tp.z, p.z - sz.z, p.z + sz.z) ? 1 : 2;
    //        angIdx = dir == 0 ? (angIdx == 0 ? 1 : angIdx == 1 ? 0 : angIdx == 2 ? 3 : 2) :
    //            dir == 1 ? (angIdx == 0 ? 3 : angIdx == 1 ? 2 : angIdx == 2 ? 1 : 0) :
    //            (angIdx == 0 ? 2 : angIdx == 1 ? 3 : angIdx == 2 ? 0 : 1);
    //        Te = V3.Y(angs[angIdx]);
    //    }
    //}
    private void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.PowerUp)) {
            PowerUp pwrUp = other.Gc<PowerUp>();
            if (pwrUp.tp == PowerUpTp.Zoom) {
                if (isPlayer) Ls.I.isPlayerZoom = true;
                else Ls.I.isBotZoom = true;
            } else if (pwrUp.tp == PowerUpTp.Mul3) {
                CrtBall(Ls.I.mul3Ang);
                CrtBall(-Ls.I.mul3Ang);
            } else if (pwrUp.tp == PowerUpTp.BallSpd) {
                if (isPlayer) Ls.I.isPlayerBallSpd = true;
                else Ls.I.isBotBallSpd = true;
            } else {
                if (isPlayer) Ls.I.isPlayerHumanSpd = true;
                else Ls.I.isBotHumanSpd = true;
            }
            Dst(pwrUp.go);
        }
    }
    public void CrtBall(float dAng) {
        Ball ball = Ins(Ls.I.ballPf, Tp, Tr * Q.Y(dAng), Ls.I.tf);
        ball.Tls = Tls;
        ball.isPlayer = isPlayer;
        ball.RenMatCol(isPlayer ? Ls.I.l.e.playerC : Ls.I.l.e.botC);
        Ls.I.balls.Add(ball);
        //ball.Angs();
        ball.rb.V(ball.F * Spd);
    }
}