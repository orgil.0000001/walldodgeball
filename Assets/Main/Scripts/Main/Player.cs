﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using System.IO;
using System.Linq;

public class Player : Character {
    public static Player I { get { if (_.Null()) _ = GameObject.FindObjectOfType<Player>(); return _; } }
    static Player _;
    float r = 100, dis;
    private void Awake() { _ = this; }
    public void Reset() { }
    bool isSta = false;
    void Start() {
        Init();
    }
    void Update() {
        if (IsPlaying) {
            if (IsMbD) {
                isSta = true;
                mp = Mp;
                Anim("Walk");
            }
            if (isSta) {
                if (IsMb) {
                    dis = V3.Dis(Mp, mp);
                    if (dis > 10) {
                        if (dis > r)
                            mp = V3.Move(Mp, mp, r);
                        dirTf.rotation = Q.Y(Ang.LookF(mp, Mp));
                        rb.V(dirTf.forward * M.C01(dis / r) * Spd);
                    }
                }
                if (IsMbU) {
                    if (shotCnt < Ls.I.l.playerBallCnt) {
                        Anim("Throw");
                        Shot();
                    } else {
                        Anim("Idle");
                    }
                    rb.V0();
                }
            }
            UpdTf();
        } else
            rb.V0();
    }
    private void OnCollisionEnter(Collision collision) {
        if (collision.Tag(Tag.Ball)) {
            DstGo(collision.Gc<Ball>());
        }
    }
}