﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Bot : Character {
    float rotSpd = 5,
        walkTm = 0, walkDt = 0, walkMinTm = 1.5f, walkMaxTm = 3,
        curAng = 0, ang = 0,
        shotTm = 0, shotDt = 0, shotMinTm = 1.5f, shotMaxTm = 3;
    bool isSta = false, isWalk = true;
    void Start() {
        Init();
        shotTm = Rnd.Rng(shotMinTm, shotMaxTm);
    }
    void Update() {
        if (IsPlaying) {
            if (!isSta) {
                isSta = true;
                Anim("Walk");
            }
            if (isWalk) {
                walkDt += Dt;
                if (walkDt > walkTm) {
                    walkDt = 0;
                    walkTm = Rnd.Rng(walkMinTm, walkMaxTm);
                    ang = Rnd.Ang;
                }
                curAng = M.Lerp(curAng, ang, Dt * rotSpd);
                dirTf.rotation = Q.Y(curAng);
                rb.V(dirTf.forward * Spd);
            } else {
                rb.V0();
            }
            if (shotCnt < Ls.I.l.botBallCnt) {
                shotDt += Dt;
                if (shotDt > shotTm) {
                    shotDt = 0;
                    shotTm = Rnd.Rng(shotMinTm, shotMaxTm);
                    Anim("Throw");
                    isWalk = false;
                    Shot();
                    Ivk(nameof(Walk), 0.5f);
                }
            }
            UpdTf();
        } else
            rb.V0();
    }
    void Walk() {
        isWalk = true;
        Anim("Walk");
    }
    private void OnCollisionEnter(Collision collision) {
        if (collision.Tag(Tag.Wall)) {
            ang = Rnd.Rng(dirTf.Te().y + 180 - 45, dirTf.Te().y + 180 + 45);
        } else if (collision.Tag(Tag.Ball)) {
            DstGo(collision.Gc<Ball>());
        }
    }
}