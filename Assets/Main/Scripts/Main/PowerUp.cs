using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public enum PowerUpTp { Zoom, Mul3, BallSpd, HumanSpd }

public class PowerUp : Mb {
    public PowerUpTp tp;
}
