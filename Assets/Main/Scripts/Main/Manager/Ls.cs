﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

[System.Serializable]
public class EnvData {
    public Color botC, botWallC, botFloorC, playerC, playerWallC, playerFloorC, wallMidC, bgC;
    public EnvData(Color botC, Color botWallC, Color botFloorC, Color playerC, Color playerWallC, Color playerFloorC, Color wallMidC, Color bgC) {
        this.botC = botC;
        this.botWallC = botWallC;
        this.botFloorC = botFloorC;
        this.playerC = playerC;
        this.playerWallC = playerWallC;
        this.playerFloorC = playerFloorC;
        this.wallMidC = wallMidC;
        this.bgC = bgC;
    }
}

[System.Serializable]
public class Level {
    public int env, playerBallCnt, botBallCnt, playerCnt, botCnt;
    public Vector2 sz;
    public List<PowerUpTp> playerPwrUps, botPwrUps;
    public List<Vector4> playerWalls, botWalls;
    public EnvData e => Ls.I.envs[env];
    public Level(int env, int playerBallCnt, int botBallCnt, int playerCnt, int botCnt, Vector2 sz, List<PowerUpTp> playerPwrUps, List<PowerUpTp> botPwrUps, List<Vector4> playerWalls, List<Vector4> botWalls) {
        this.env = env;
        this.playerBallCnt = playerBallCnt;
        this.botBallCnt = botBallCnt;
        this.playerCnt = playerCnt;
        this.botCnt = botCnt;
        this.sz = sz;
        this.playerPwrUps = playerPwrUps;
        this.botPwrUps = botPwrUps;
        this.playerWalls = playerWalls;
        this.botWalls = botWalls;
    }
}

public class Ls : Singleton<Ls> { // Level Spawner
    public Bot bot;
    public PowerUp pwrUpPf;
    public Color pwrUpZoomC, pwrUpMul3C, pwrUpBallSpdC, pwrUpHumanSpdC;
    public Image playerImg, botImg;
    public Text playerTxt, botTxt;
    public GameObject humanPf, wallPf;
    public Ball ballPf;
    public List<EnvData> envs;
    public List<Level> levels;
    public bool useLvl = false;
    public int lvl = 1;
    int lvlCnt = 15, rndLvlMin = 1, rndLvlMax = 15;
    //int Lvl => GetLvl(Gc.Level, lvlCnt, rndLvlMin, rndLvlMax);
    int Lvl => GetLvl(Gc.Level, levels.Count, 1, levels.Count);
    int LvlIdx => Lvl - 1;
    public float wallH = 2, wallW = 1, wallMidW = 0.5f, spc = 2, humanSpd = 3, humanSpd2 = 6, humanLocSpd = 2, ballSpd = 3, ballSpd2 = 6, ballMinRot = 10, ballSz = 0.5f, ballSz2 = 0.75f, mul3Ang = 20, ballCrtY = 1;
    [HideInInspector]
    public float ballLim;
    [HideInInspector]
    public List<Ball> balls = new List<Ball>();
    [HideInInspector]
    public Level l;
    [HideInInspector]
    public bool isPlayerZoom = false, isPlayerBallSpd = false, isPlayerHumanSpd = false, isBotZoom = false, isBotBallSpd = false, isBotHumanSpd = false;
    public Vector3 Zoom(bool isPlayer) { return V3.V((isPlayer ? isPlayerZoom : isBotZoom) ? ballSz2 : ballSz); }
    public float BallSpd(bool isPlayer) { return (isPlayer ? isPlayerBallSpd : isBotBallSpd) ? ballSpd2 : ballSpd; }
    public float HumanSpd(bool isPlayer) { return (isPlayer ? isPlayerHumanSpd : isBotHumanSpd) ? humanSpd2 : humanSpd; }
    public void Init() {
        if (useLvl)
            Gc.Level = lvl;
        LoadLevel();
        LeaderBoardData.SetDatas();
    }
    public void LoadLevel() {
        ballLim = wallMidW / 2 + ballSz2;
        l = levels[LvlIdx];
        botImg.color = botTxt.color = l.e.botC;
        playerImg.color = playerTxt.color = l.e.playerC;
        Vector3 wallLrSz = V3.V(wallW, wallH, l.sz.y), wallLrSz2 = wallLrSz.Z((l.sz.y + wallW) * 2), wallFSz = V3.V(l.sz.x + wallW * 2, wallH, wallW), floorSz = l.sz.Xz(1), wallMidSz = V3.V(l.sz.x, wallH, wallMidW), wallLrP = V3.V(l.sz.x + wallW, wallH, l.sz.y) / 2, wallFP = V3.Yz(wallH / 2, l.sz.y + wallW / 2);
        TfC(l.e.botFloorC, floorSz.Znp() / 2, floorSz, "0 0 0");
        TfC(l.e.botWallC, wallLrP.Npp(), wallLrSz, "0 0 1");
        TfC(l.e.botWallC, wallLrP, wallLrSz, "0 0 2");
        TfC(l.e.botWallC, wallFP, wallFSz, "0 0 3");
        TfC(l.e.playerFloorC, floorSz.Znn() / 2, floorSz, "0 1 0");
        TfC(l.e.playerWallC, wallLrP.Npn(), wallLrSz, "0 1 1");
        TfC(l.e.playerWallC, wallLrP.Ppn(), wallLrSz, "0 1 2");
        TfC(l.e.playerWallC, wallFP.Ppn(), wallFSz, "0 1 3");
        TfC(l.e.wallMidC, wallLrP.Zpz(), wallMidSz, "0 2");
        TfC(l.e.wallMidC, wallLrP.Npz(), wallLrSz2, "0 3");
        TfC(l.e.wallMidC, wallLrP.Ppz(), wallLrSz2, "0 4");
        for (int i = 0; i < 2; i++) {
            bool isPlayer = i == 0;
            List<PowerUpTp> pwrUps = isPlayer ? l.playerPwrUps : l.botPwrUps;
            for (int j = 0; j < pwrUps.Count; j++) {
                PowerUpTp tp = pwrUps[j];
                PowerUp pwrUp = Ins(pwrUpPf, V3.V(Rnd.Rng(0.5f - l.sz.x / 2, l.sz.x / 2 - 0.5f), ballCrtY, isPlayer.Sign() * Rnd.Rng(wallMidW / 2 + 0.5f, l.sz.y - 0.5f)), Q.O, tf);
                GameObject cGo = pwrUp.ChildGo((int)tp);
                cGo.Show();
                cGo.Childs().ForEach(x => x.RenMatCol(tp == PowerUpTp.Zoom ? pwrUpZoomC : tp == PowerUpTp.Mul3 ? pwrUpMul3C : tp == PowerUpTp.BallSpd ? pwrUpBallSpdC : pwrUpHumanSpdC));
                pwrUp.tp = tp;
            }
            List<Vector4> walls = isPlayer ? l.playerWalls : l.botWalls;
            Color wallC = isPlayer ? l.e.playerWallC : l.e.botWallC;
            for (int j = 0; j < walls.Count; j++) {
                GameObject wallGo = Ins(wallPf, V3.V(walls[j].x, wallH / 2, walls[j].y), Q.O, tf);
                wallGo.Tls(walls[j].z, wallH, walls[j].w);
                wallGo.RenMatCol(wallC);
            }
        }
        float f = M.Apx(Screen.height.F() / Screen.width, 2.165f, 0.01f) ? -1.7f : // IPhone X
            M.Apx(Screen.height.F() / Screen.width, 1.333f, 0.01f) ? -1.4f : // IPad
            -1.5f; // IPhone
        Cm.I.Tp = Cm.I.F * (l.sz + V2.V(wallW * 2)).magnitude * f;
        Camera.main.backgroundColor = l.e.bgC;
        Player.I.Tp = V3.Z(-l.sz.y / 2);
        Player.I.Init(l.playerCnt);
        bot.Tp = V3.Z(l.sz.y / 2);
        //bot = Bs.I.Create(V3.Z(l.sz.y / 2), V3.O);
        bot.Init(l.botCnt);
    }
    void TfC(Color c, Vector3 p, Vector3 s, string childs) {
        GameObject cGo = go.ChildGo(childs);
        cGo.Tlp(p);
        cGo.Tls(s);
        cGo.RenMatCol(c);
    }
    int GetLvl(int lvl, int lvlCnt, int rnd1, int rnd2) {
        if (Data.LevelData.S().IsNe())
            Data.LevelData.SetList(A.ListAp(1, 1, lvlCnt));
        List<int> lis = Data.LevelData.ListI();
        if (lvl <= lis.Count) {
            return lis[lvl - 1];
        } else {
            int prvLvl = lis.Last();
            for (int i = lis.Count + 1; i <= lvl; i++) {
                prvLvl = prvLvl == rnd1 ? Rnd.RngIn(rnd1 + 1, rnd2) : prvLvl == rnd2 ? Rnd.RngIn(rnd1, rnd2 - 1) : Rnd.P((prvLvl - rnd1).F() / (rnd2 - rnd1)) ? Rnd.RngIn(rnd1, prvLvl - 1) : Rnd.RngIn(prvLvl + 1, rnd2);
                lis.Add(prvLvl);
            }
            Data.LevelData.SetList(lis);
            return prvLvl;
        }
    }
}
// ░ ▒ ▓ █ ▄ ▀ ■
// ┌ ┬ ┐ ─ ╔ ╦ ╗ ═
// ├ ┼ ┤ │ ╠ ╬ ╣ ║
// └ ┴ ┘   ╚ ╩ ╝
// ⁰¹²³⁴⁵⁶⁷⁸⁹ ⁻⁺⁼⁽⁾ ⁱⁿ superscript
// ₀₁₂₃₄₅₆₇₈₉ ₋₊₌₍₎⨧ ᵣᵤₐᵢⱼₓₑᵥₒₔ ᵪᵧᵦᵨᵩ subscript
// _ ¯ ~ ≡ ‗ ¦ ¨ ¬ · |
// ñ Ñ @ ¿ ? ¡ ! : / \ frequently-used
// á é í ó ú Á É Í Ó Ú vowels acute accent
// ä ë ï ö ü Ä Ë Ï Ö Ü vowels with diaresis
// ½ ¼ ¾ ¹ ³ ² ƒ ± × ÷ mathematical symbols
// $ £ ¥ ¢ ¤ ® © ª º ° commercial / trade symbols
// " ' ( ) [ ] { } « » quotes and parenthesis