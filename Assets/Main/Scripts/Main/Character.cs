﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using TMPro;

public class Character : Mb {
    public LeaderBoardData data;
    HWP hwp = null;
    TextMeshPro followTmp;
    protected List<GameObject> gos = new List<GameObject>();
    protected List<Vector3> pnts = new List<Vector3>();
    protected Transform dirTf;
    protected int shotCnt = 0;
    public float Spd => Ls.I.HumanSpd(data.isPlayer);
    public void Init() {
        tf.Par(Bs.I.transform);
        dirTf = go.Child(0);
        rb.NoG();
        rb.Con(false, true, false, true, true, true);
    }
    public void Init(int n) {
        Txt();
        for (int i = 0; i < n; i++)
            gos.Add(CrtGo());
        UpdPnts(true);
    }
    GameObject CrtGo() {
        GameObject crtGo = Ins(Ls.I.humanPf, Tp, Tr, tf);
        crtGo.Child(0).RenMats().ForEach(x => x.color = data.isPlayer ? Ls.I.l.e.playerC : Ls.I.l.e.botC);
        return crtGo;
    }
    public void DstGo(Ball ball) {
        int i = gos.PosNearIdx(ball.Tp);
        GameObject dstGo = gos[i];
        Ls.I.balls.Rmv(ball);
        Dst(ball.go);
        gos.RmvAt(i);
        dstGo.Par(Ls.I.tf);
        Dst(dstGo.Cc());
        dstGo.An().Play("Death");
        pnts.RmvAt(i);
        //UpdPnts(false);
        if (gos.IsEmpty()) {
            Ls.I.balls.ForEach(x => Dst(x.go));
            if (data.isPlayer) {
                Gc.I.GameOver();
                Ls.I.bot.Anim("Victory");
            } else {
                Gc.I.LevelCompleted();
                Player.I.Anim("Victory");
            }
        }
    }
    public void Anim(string name) {
        gos.ForEach(x => x.An().Play(name));
    }
    protected void Txt() {
        if (data.isPlayer)
            Ls.I.playerTxt.text = shotCnt + "/" + Ls.I.l.playerBallCnt;
        else
            Ls.I.botTxt.text = shotCnt + "/" + Ls.I.l.botBallCnt;
    }
    protected void Shot() {
        shotCnt++;
        Txt();
        gos.ForEach(x => Shot(x.Tp()));
    }
    public void Shot(Vector3 p) {
        Ball ball = Ins(Ls.I.ballPf, p.Y(Ls.I.ballCrtY), dirTf.rotation, Ls.I.tf);
        ball.Tls = V3.V(Ls.I.ballSz);
        ball.isPlayer = data.isPlayer;
        ball.RenMatCol(data.isPlayer ? Ls.I.l.e.playerC : Ls.I.l.e.botC);
        Ls.I.balls.Add(ball);
        ball.Init();
    }
    protected void UpdTf() {
        for (int i = 0; i < gos.Count; i++) {
            gos[i].Tr(dirTf.rotation);
            gos[i].Tlp(V3.Move(gos[i].Tlp(), pnts[i], Dt * Ls.I.humanLocSpd));
        }
    }
    protected void UpdPnts(bool isUpd) {
        if (gos.Count == 1) {
            pnts = List(V3.O);
        } else if (gos.Count == 2) {
            pnts = List(V3.X(-Ls.I.spc / 2f), V3.X(Ls.I.spc / 2f));
        } else {
            float r = Ls.I.spc / M.Sqrt(2 - 2 * M.Cos(360f / gos.Count));
            pnts = new List<Vector3>();
            for (int i = 0; i < gos.Count; i++)
                pnts.Add(Ang.Xz(360f * i / gos.Count + (data.isPlayer ? 90 : -90), r));
        }
        if (isUpd) {
            for (int i = 0; i < gos.Count; i++) {
                gos[i].Tlp(pnts[i]);
                gos[i].TleY(data.isPlayer ? 0 : 180);
            }
        }
    }
    public void UpdatePlace(int place) { }
    public void UpdateColor(Color col) { }
    // default functions
    public void CreateName(LeaderBoardData lbd) {
        UpdateColor(lbd.col);
        data = lbd;
        if (Gc.I.followType != FollowType.None) {
            hwp = go.Gc<HWP>();
            if (hwp) {
                hwp.info.arrow.size = Screen.width * 0.06f;
                HWPManager.I.TextStyle.fontSize = M.RoundI(Screen.width * 0.06f);
                HWPManager.I.TextStyle.contentOffset = V2.u * -(Screen.width * 0.06f * 1.6f);
                hwp.info.text = Gc.I.isNameUppercase ? data.name.ToUpper() : data.name;
                hwp.info.color = data.col;
                hwp.info.character = this;
            }
            if (Gc.I.followType == FollowType.Follow || Gc.I.followType == FollowType.FollowPointer) {
                Follow follow = Crt.Pf<Follow>(A.LoadGo("Main/FollowName"), new Tf(Tp), Gc.I.transform);
                follow.followTf = transform;
                followTmp = follow.Child<TextMeshPro>(0);
                if (followTmp) {
                    followTmp.text = Gc.I.isNameUppercase ? data.name.ToUpper() : data.name;
                    followTmp.color = data.col;
                }
            }
        }
    }
    public void RemoveName() {
        if (Gc.I.followType != FollowType.None) {
            hwp.info.text = "";
            hwp.info.arrow.size = 0;
            if ((Gc.I.followType == FollowType.Follow || Gc.I.followType == FollowType.FollowPointer) && followTmp)
                followTmp.text = "";
        }
    }
}