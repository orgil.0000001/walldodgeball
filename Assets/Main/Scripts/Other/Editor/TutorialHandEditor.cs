using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using Orgil;

[CustomEditor(typeof(TutorialHand))]
public class TutorialHandEditor : Editor {
    TutorialHand hand;

    void OnEnable() {
        hand = (TutorialHand)target;
    }

    void OnSceneGUI() {
        Handles.color = C._b;
        List<Vector3> lis = Crv.Crs(hand.pnts, hand.smt, hand.spc);
        for (int i = 1; i < lis.Count; i++)
            Handles.DrawLine(hand.TfPnt(lis[i - 1]), hand.TfPnt(lis[i]));

        Handles.color = C.r;
        for (int i = 0; i < hand.pnts.Count; i++)
            hand.pnts[i] = hand.TfInvPnt(
                Handles.FreeMoveHandle(
                    hand.TfPnt(hand.pnts[i]),
                    Q.O, 25, V3.O, Handles.CylinderHandleCap
                )
            );
    }
}