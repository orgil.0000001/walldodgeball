namespace Orgil {
	public enum Tag { Untagged, Respawn, Finish, EditorOnly, MainCamera, Player, GameController, Bot, Ball, Human, Diamond, Wall, PowerUp }
	public class Lyr { public const int Default = 0, TransparentFX = 1, IgnoreRaycast = 2, Water = 4, UI = 5, Wall = 6, WallMid = 7, Ball = 8, BallInit = 9, Human = 10, PowerUp = 11; }
	public class Lm { public const int Default = 1, TransparentFX = 2, IgnoreRaycast = 4, Water = 16, UI = 32, Wall = 64, WallMid = 128, Ball = 256, BallInit = 512, Human = 1024, PowerUp = 2048; }
}