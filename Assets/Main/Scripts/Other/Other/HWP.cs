﻿using UnityEngine;
using Orgil;

public enum HWPType { None, Inc, Dec }
public class HWP : Mb {
    public HWPInfo info;
    void Start() {
        if (HWPManager.I) {
            if (!info.m_Target)
                info.m_Target = tf;
            if (info.ShowDynamically)
                info.Hide = true;
            HWPManager.I.CreateHud(info);
        } else
            Debug.LogError("Need have a Hud Manager in scene");
    }
    public void Show() {
        if (HWPManager.I)
            HWPManager.I.HideStateHud(info, false);
        else
            Debug.LogWarning("the instance of bl_HudManager in scene wasn't found.");
    }
    public void Hide() {
        if (HWPManager.I)
            HWPManager.I.HideStateHud(info, true);
        else
            Debug.LogWarning("the instance of bl_HudManager in scene wasn't found.");
    }
}
[System.Serializable]
public class HWPInfo {
    public Character character;
    public string text = null;
    [Tooltip("Transform to HUD follow, is empty this will be take this tf.")]
    public Transform m_Target = null;
    public Texture2D m_Icon = null;
    public Color color = new Color(1, 1, 1, 1);
    [Tooltip("Modify the target position.")]
    public Vector3 Offset;
    [Tooltip("When player is it approaches to the target, the icon It becomes smaller (Decreasing) or large(Increasing).")]
    public HWPType m_TypeHud = HWPType.Dec;
    [Tooltip("Max size to the hud can scale.")]
    public float m_MaxSize = 50f;
    [Range(0.1f, 10), Tooltip("Hide when target is more close that the distance, leave 0 for no hide")]
    public float HideOnCloseDistance = 0;
    [Range(15, 1000), Tooltip("Hide when target is more large that the distance, leave 0 for no hide")]
    public float HideOnLargeDistance = 0;
    public bool ShowDistance = true;
    [Tooltip("Is HUD hide for default?, you can change it in runtime.")]
    public bool ShowDynamically = false;
    [Tooltip("hud is fade.")]
    public bool isPalpitin = true;
    [System.Serializable]
    public class m_Arrow {
        public bool ShowArrow = true;
        public Texture ArrowIcon = null;
        public Vector3 ArrowOffset = V3.O;
        public float size = 30;
    }
    [Space(5)]
    [Header("Arrow")]
    public m_Arrow arrow;
    [HideInInspector]
    public bool tip = false;
    [HideInInspector]
    public bool Hide = false;
}
public class HWPRayHelper : Mb {
    public float disCheck = 50f;
    private HWP cacheHud = null;
    void Update() {
        RaycastHit hit;
        Dbg.Ray(Tp, F, C.g);
        if (Physics.Raycast(Tp, F, out hit, disCheck)) {
            if (hit.transform.Gc<HWP>()) {
                if (hit.transform.Gc<HWP>().info.ShowDynamically) {
                    cacheHud = hit.transform.Gc<HWP>();
                    cacheHud.Show();
                }
            }
        } else if (cacheHud) {
            cacheHud.Hide();
            cacheHud = null;
        }
    }
}
public static class HWPUtility {
    public static float GetRot(float x1, float y1, float x2, float y2) {
        float difX = x2 - x1, difY = y2 - y1, atan = M.Atan(difY / difX);
        if (difX < 0)
            atan += 180;
        return atan;
    }
    public static Vector2 GetPivot(float x, float y, float size) {
        float hor = x - (mCam.pixelWidth * 0.5f), ver = y - (mCam.pixelHeight * 0.5f), slope = ver / hor, h;
        Vector2 v = V2.O;
        if ((slope > GetScreenSlope) || (slope < -GetScreenSlope)) {
            h = (MidH - HalfSz(size)) / ver;
            if (ver < 0) {
                v.y = HalfSz(size);
                h *= -1;
            } else
                v.y = mCam.pixelHeight - HalfSz(size);
            v.x = MidW + (hor * h);
            return v;
        }
        h = (MidW - HalfSz(size)) / hor;
        if (hor < 0) {
            v.x = HalfSz(size);
            h *= -1;
        } else
            v.x = mCam.pixelWidth - HalfSz(size);
        v.y = MidH + (ver * h);
        return v;
    }
    public static float GetScreenSlope { get => (float)mCam.pixelHeight / mCam.pixelWidth; }
    public static float MidH { get => Camera.main.pixelHeight / 2; }
    public static float MidW { get => Camera.main.pixelWidth / 2; }
    public static float HalfSz(float s) { return s / 2; }
    public static Camera mCam { get => Camera.main ? Camera.main : Camera.current; }
    public static Vector2 Marge(Vector2 v, float size) {
        float globalMarge = 50f;
        Vector2 scrSz = V2.V(Screen.width * 0.5f, Screen.height * 0.5f);
        return V2.V((v.x + size < scrSz.x).Sign() * M.C(scrSz.x - v.x, globalMarge), (v.y + size < scrSz.y).Sign() * M.C(scrSz.y - v.y, globalMarge));
    }
    public static Rect SclRect(Rect r) {
        float retine = RetineAspect;
        r.width = r.width * retine;
        r.height = r.height * retine;
        r.x = r.x * retine;
        r.y = r.y * retine;
        return r;
    }
    public static float RetineAspect { get => M.Pow2(M.Lerp(M.Lb((float)Screen.width / HWPManager.I.m_ReferenceResolution.x), M.Lb((float)Screen.height / HWPManager.I.m_ReferenceResolution.y), HWPManager.I.m_MatchWidthOrHeight)); }
    public static Vector3 ScreenPos(Transform t) {
        if (mCam) {
            Vector3 p = mCam.WorldToScreenPoint(t.position);
            return V3.V(p.x / mCam.pixelWidth, p.y / mCam.pixelHeight, t.position.z);
        } else
            return V3.I * 0.5f;
    }
    public static bool IsOnScreen(Vector3 pos, Transform t) {
        Vector3 mDirection = t.position - mCam.Tp();
        if (V3.Dot(mDirection, mCam.transform.forward) <= 0)
            return false;
        float margen = 0.001f;
        return !(pos.x < margen || pos.x > 1 - margen || pos.y < margen || pos.y > 1 - margen);
    }
}