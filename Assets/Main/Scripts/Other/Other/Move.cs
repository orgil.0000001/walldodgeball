﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public enum PntTp { None, BzrCrv, Crs }
public enum CrsTp { None, StaEnd, StaEndAdd, Loop }

public class Move : Mb {
    public bool isPlayMv = true, isUpdPnts = false, isDraw = true;
    public List<Vector3> pnts;
    public float spc = 0.01f;
    public int spd = 5;
    public bool isLoop = false;
    public PntTp tp;
    public CrsTp crsTp;
    public int crsSmt = 10;
    public Vector3 crsSta, crsEnd;
    public int bzrN = 1000;
    int i = 0;
    List<Vector3> lis = new List<Vector3>();
    private void Start() {
        UpdPnts();
    }
    private void Update() {
        if (isPlayMv ? IsPlaying : true) {
            if (isUpdPnts)
                UpdPnts();
            if (isDraw)
                Dbg.Crv(Tf.New, lis, C.I);
            Tp = lis[M.RepPingPongIdx(i = M.RepIdx(i + spd, lis.Count * 2), lis.Count, isLoop)];
        }
    }
    void UpdPnts() {
        lis.Clear();
        if (tp == PntTp.BzrCrv) {
            lis = Crv.BzrCrv(pnts, bzrN, spc);
        } else if (tp == PntTp.Crs) {
            lis = Crv.Crs(pnts, crsSmt, spc, crsTp, crsSta, crsEnd);
        } else {
            lis = Crv.PntsSameDis(isLoop ? pnts.Copy().Add1(pnts[0]) : pnts, spc);
        }
    }
    public void Crs(List<Vector3> pnts, float spc, int spd, CrsTp crsTp, int smt, Vector3 sta = default, Vector3 end = default) {
        tp = PntTp.Crs;
        this.pnts = pnts;
        this.spc = spc;
        this.spd = spd;
        this.crsTp = crsTp;
        crsSmt = smt;
        crsSta = sta;
        crsEnd = end;
        isLoop = crsTp == CrsTp.Loop;
        UpdPnts();
    }
    public void BzrCrv(List<Vector3> pnts, float spc, int spd, int n) {
        tp = PntTp.BzrCrv;
        this.pnts = pnts;
        this.spc = spc;
        this.spd = spd;
        bzrN = n;
        isLoop = false;
        UpdPnts();
    }
    public void Pnts(List<Vector3> pnts, float spc, int spd, bool isLoop) {
        tp = PntTp.None;
        this.pnts = pnts;
        this.spc = spc;
        this.spd = spd;
        this.isLoop = isLoop;
        UpdPnts();
    }
}
