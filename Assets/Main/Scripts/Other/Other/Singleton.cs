using UnityEngine;
using Orgil;

public class Singleton<T> : Mb where T : Mb {
    private static object lockObj = new object();
    private static T _;
    public static T I {
        get {
            lock (lockObj) {
                if (!_) {
                    _ = (T)FindObjectOfType(typeof(T));
                    if (!_) {
                        var obj = new GameObject();
                        _ = obj.Ac<T>();
                        obj.name = typeof(T).tS();
                        DontDestroyOnLoad(obj);
                    }
                }
                return _;
            }
        }
    }
}