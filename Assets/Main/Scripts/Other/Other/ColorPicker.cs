﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class ColorPicker : Mb {
    public Color Color { get { return color; } set { isSet = true; Setup(value); } }
    [SerializeField]
    Color color = C.I;
    bool isSet = false;
    Action update;
    static bool GetLocalMouse(GameObject go, out Vector2 result) {
        RectTransform rt = go.Rt();
        Vector3 mp = rt.TfInvPnt(Mp);
        result.x = M.C(mp.x, rt.rect.min.x, rt.rect.max.x);
        result.y = M.C(mp.y, rt.rect.min.y, rt.rect.max.y);
        return rt.rect.Contains(mp);
    }
    static Vector2 GetWidgetSize(GameObject go) { return go.Rt().rect.size; }
    static void RGBToHSV(Color color, out float h, out float s, out float v) {
        float cmin = M.Min(color.r, color.g, color.b), cmax = M.Max(color.r, color.g, color.b), d = cmax - cmin;
        h = d == 0 ? 0 : cmax == color.r ? M.Rep_((color.g - color.b) / d, 6) : cmax == color.g ? (color.b - color.r) / d + 2 : (color.r - color.g) / d + 4;
        s = cmax == 0 ? 0 : d / cmax;
        v = cmax;
    }
    void Setup(Color inputColor) {
        GameObject satValGo = go.FndGo("Data/SaturationValue"), satValKnobGo = go.FndGo("Data/SaturationValue/Knob"), hueGo = go.FndGo("Data/Hue"), hueKnob = go.FndGo("Data/Hue/Knob"), alphaGo = go.FndGo("Data/Alpha"), alphaKnobGo = go.FndGo("Data/Alpha/Knob"), resultGo = go.FndGo("Result/Result"), dataGo = go.FndGo("Data");
        // hue texture apply
        Color[] hueCols = Arr(C.r, C.y, C.g, C.c, C.b, C.m), alphaCols = Arr(C.Rgba(1, 1, 1, 0), C.I), satValCols = Arr(C._b, C._b, C.I, hueCols[0]);
        Texture2D hueTex = new Texture2D(1, 7), alphaTex = new Texture2D(2, 1), satValTex = new Texture2D(2, 2);
        for (int i = 0; i < 7; i++)
            hueTex.SetPixel(0, i, hueCols[i % 6]);
        hueTex.Apply();
        hueGo.Img().sprite = Sprite.Create(hueTex, new Rect(0, 0.5f, 1, 6), V2.V(0.5f, 0.5f));
        // alpha texture apply
        for (int i = 0; i < 2; i++)
            alphaTex.SetPixel(i, 0, alphaCols[i]);
        alphaTex.Apply();
        alphaGo.Img().sprite = Sprite.Create(alphaTex, new Rect(0.5f, 0, 1, 1), V2.V(0.5f, 0.5f));
        // saturation value texture apply
        satValGo.Img().sprite = Sprite.Create(satValTex, new Rect(0.5f, 0.5f, 1, 1), V2.V(0.5f, 0.5f));
        Action resetSatValTexAct = () => {
            for (int j = 0; j < 2; j++)
                for (int i = 0; i < 2; i++)
                    satValTex.SetPixel(i, j, satValCols[i + j * 2]);
            satValTex.Apply();
        };
        // load size
        Vector2 hueSz = GetWidgetSize(hueGo), alphaSz = GetWidgetSize(alphaGo), satValSz = GetWidgetSize(satValGo);
        float hue, saturation, value, alpha = inputColor.a;
        RGBToHSV(inputColor, out hue, out saturation, out value);
        Action applyH = () => {
            int i0 = M.C((int)hue, 5), i1 = (i0 + 1) % 6;
            satValCols[3] = C.Lerp(hueCols[i0], hueCols[i1], hue - i0);
            resetSatValTexAct();
        };
        Action applySv = () => {
            Vector2 sv = V2.V(saturation, value), isv = V2.V(1 - sv.x, 1 - sv.y);
            Color c0 = isv.x * isv.y * satValCols[0], c1 = sv.x * isv.y * satValCols[1], c2 = isv.x * sv.y * satValCols[2], c3 = sv.x * sv.y * satValCols[3], resCol = c0 + c1 + c2 + c3;
            Image resImg = resultGo.Img();
            resCol = isSet ? inputColor : resCol.A(alpha);
            isSet = false;
            resImg.color = resCol;
            color = resCol;
        };
        applyH();
        applySv();
        satValKnobGo.transform.localPosition = V2.V(saturation * satValSz.x, value * satValSz.y);
        hueKnob.transform.localPosition = V2.V(hueKnob.transform.localPosition.x, hue / 6 * satValSz.y);
        alphaKnobGo.transform.localPosition = V2.V(alpha * alphaSz.x, alphaKnobGo.transform.localPosition.y);
        Action dragH = null, dragSv = null, dragA = null, idle = () => {
            if (IsMbD) {
                Vector2 mp;
                if (GetLocalMouse(resultGo, out mp)) {
                    dataGo.SetActive(!dataGo.activeSelf);
                } else if (dataGo.activeSelf) {
                    if (GetLocalMouse(hueGo, out mp))
                        update = dragH;
                    else if (GetLocalMouse(satValGo, out mp))
                        update = dragSv;
                    else if (GetLocalMouse(alphaGo, out mp))
                        update = dragA;
                }
            }
        };
        dragA = () => {
            Vector2 mp;
            GetLocalMouse(alphaGo, out mp);
            alpha = mp.x / alphaSz.x;
            Image resImg = resultGo.Img();
            Color resCol = C.Rgba(resImg.color.r, resImg.color.g, resImg.color.b, alpha);
            resImg.color = resCol;
            color = resCol;
            alphaKnobGo.transform.localPosition = V2.V(mp.x, alphaKnobGo.transform.localPosition.y);
            if (IsMbU)
                update = idle;
        };
        dragH = () => {
            Vector2 mp;
            GetLocalMouse(hueGo, out mp);
            hue = mp.y / hueSz.y * 6;
            applyH();
            applySv();
            hueKnob.transform.localPosition = V2.V(hueKnob.transform.localPosition.x, mp.y);
            if (IsMbU)
                update = idle;
        };
        dragSv = () => {
            Vector2 mp;
            GetLocalMouse(satValGo, out mp);
            saturation = mp.x / satValSz.x;
            value = mp.y / satValSz.y;
            applySv();
            satValKnobGo.transform.localPosition = mp;
            if (IsMbU)
                update = idle;
        };
        update = idle;
    }
    void Awake() {
        Color = C.I;
    }
    void Update() {
        update();
    }
}