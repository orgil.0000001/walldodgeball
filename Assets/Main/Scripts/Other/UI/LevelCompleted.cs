﻿using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class LevelCompleted : Mb {
    public FormatTxt level;
    public LeaderBoard leaderBoard;
    public float time = 1;
    public void Data(int level, List<LeaderBoardData> datas) {
        A.Anim(go, new List<Fo>() { Fo.F(0).S(0), Fo.F(30).S(1.1f), Fo.F(40).S(1) });
        if (this.level)
            this.level.Data(level);
        if (leaderBoard)
            leaderBoard.Data(datas);
        if (time > 0)
            Cc.I.Replay(time);
    }
}